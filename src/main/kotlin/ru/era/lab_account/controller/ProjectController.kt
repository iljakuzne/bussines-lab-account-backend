package ru.era.lab_account.controller

import org.springframework.http.MediaType
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import ru.era.lab_account.domain.view.ProjectCardRating
import ru.era.lab_account.dto.product_with_project.ProductWithProjectDto
import ru.era.lab_account.dto.project.CreateProjectDto
import ru.era.lab_account.dto.project.ProjectCardDto
import ru.era.lab_account.dto.project.ProjectRatingDto
import ru.era.lab_account.models.pagination.PaginationRequest
import ru.era.lab_account.models.project.CreateProjectWithUserId
import ru.era.lab_account.services.crud_service.project.ProjectService
import ru.era.lab_account.services.crud_service.user_principal.UserTokenDecoder

@RestController
@RequestMapping("project")
class ProjectController(private val projectService: ProjectService,
                        private val decoder: UserTokenDecoder) {

    @PostMapping("create",produces = [MediaType.APPLICATION_JSON_VALUE])
    fun createProject (@RequestBody dto: CreateProjectDto) : Mono<Long> {
        return decoder.getUserIdByToken()
            .flatMap {userId->
                projectService.createProjectTemplate(CreateProjectWithUserId(dto,userId))
            }
    }

    @GetMapping("cards", produces =  [MediaType.APPLICATION_JSON_VALUE])
    fun getActualCards (@RequestParam("limit",required = true) limit: Int,
                        @RequestParam("offset",required = true) offset: Int) : Flux<ProjectCardDto> {
        return projectService.findProjectsForCard(PaginationRequest(limit,offset))
    }

    @GetMapping("byId1/{projectId}", produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getProductV1 (@PathVariable("projectId") projectId: Long) : Mono<ProjectCardDto> {
        return projectService.findProjectWithProductV1(projectId)
    }

    @GetMapping("byId2/{projectId}", produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getProductV2 (@PathVariable("projectId") projectId: Long) : Mono<ProductWithProjectDto> {
        return projectService.findProjectWithProductV2(projectId)
    }

    @GetMapping("for_user")
    fun getProjectForUser (@RequestParam("limit",required = true) limit: Int,
                           @RequestParam("offset",required = true) offset: Int) : Flux<ProjectCardDto>{
        return projectService.findProjectForUser(PaginationRequest(limit,offset))
    }

    @GetMapping("for_stateholder")
    @PreAuthorize("hasRole('STAKEHOLDER')")
    fun getProjectForStateHolder (@RequestParam("limit",required = true) limit: Int,
                                  @RequestParam("offset",required = true) offset: Int) : Flux<ProjectCardDto>{
        return projectService.findProjectForStateholder(PaginationRequest(limit,offset))
    }

    @GetMapping("byId/{projectId}")
    fun getProjectById (@PathVariable("projectId") projectId: Long): Mono<CreateProjectDto> {
        return projectService.findProjectById(projectId)
    }

    @GetMapping("projectRating")
    fun getProjectsByRating () : Flux<ProjectRatingDto> {
        return projectService.findProjectsWithRating()
    }
}