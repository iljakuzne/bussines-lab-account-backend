package ru.era.lab_account.controller

import org.springframework.http.MediaType.APPLICATION_JSON_VALUE
import org.springframework.web.bind.annotation.*
import reactor.core.publisher.Mono
import ru.era.lab_account.dto.market.CreateMarketDto
import ru.era.lab_account.services.crud_service.market.MarketService

@RestController
@RequestMapping("market")
class MarketController (private val marketService: MarketService) {

    @PostMapping("create", produces = [APPLICATION_JSON_VALUE])
    fun createMarket (@RequestBody dto: CreateMarketDto) : Mono<Long>{
        return marketService.createMarket(dto)
    }

    @GetMapping("byId/{marketId}")
    fun getMarketById (@PathVariable("marketId") marketId: Long) : Mono<CreateMarketDto>{
        return marketService.getMarketById(marketId)
    }

    @GetMapping("byProject/{projectId}")
    fun getByProjectId (@PathVariable("projectId") projectId: Long) : Mono<CreateMarketDto>{
        return marketService.getByProjectId(projectId);
    }
}