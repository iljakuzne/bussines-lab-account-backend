package ru.era.lab_account.controller

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Flux
import ru.era.lab_account.dto.helper.KeyValueDto
import ru.era.lab_account.services.crud_service.ways_advertising.WaysAdvertisingService

@RestController
@RequestMapping("ways_advertising")
class WaysAdvertisingController(private val waysAdvertisingService: WaysAdvertisingService) {

    @GetMapping("find_all")
    fun findAll () : Flux<KeyValueDto> {
        return waysAdvertisingService.findAll()
    }
}