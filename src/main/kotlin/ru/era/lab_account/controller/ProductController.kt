package ru.era.lab_account.controller

import org.springframework.http.MediaType.APPLICATION_JSON_VALUE
import org.springframework.web.bind.annotation.*
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import ru.era.lab_account.dto.product.CreateProductDto
import ru.era.lab_account.dto.product.ProductCardDto
import ru.era.lab_account.dto.product_with_project.ProductWithProjectDto
import ru.era.lab_account.models.pagination.PaginationRequest
import ru.era.lab_account.services.crud_service.product.ProductService

@RestController
@RequestMapping("product")
class ProductController (private val productService: ProductService) {

    @PostMapping("create",produces = [APPLICATION_JSON_VALUE])
    fun createProduct (@RequestBody createProductDto: CreateProductDto) : Mono<Long> {
        return productService.createProduct(createProductDto)
    }

    @GetMapping("actual", produces = [APPLICATION_JSON_VALUE])
    fun getActualProduct (@RequestBody paginationRequest: PaginationRequest) : Flux<ProductCardDto>{
        return productService.findActualProduct(paginationRequest)
    }

    @GetMapping("byId/{productId}")
    fun getProductById (@PathVariable("productId") productId: Long) : Mono<CreateProductDto>{
        return productService.getById(productId)
    }

    @GetMapping("byProject/{projectId}")
    fun getProductByProject (@PathVariable("projectId") projectId: Long) : Mono<CreateProductDto>{
        return productService.getByProject(projectId)
    }

}