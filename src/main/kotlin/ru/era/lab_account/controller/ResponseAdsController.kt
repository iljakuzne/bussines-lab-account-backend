package ru.era.lab_account.controller

import org.springframework.web.bind.annotation.*
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import ru.era.lab_account.dto.ads_response.AdsResponseDto
import ru.era.lab_account.services.crud_service.response_ads.ResponseAdsService

@RestController
@RequestMapping("response_ads")
class ResponseAdsController(private val responseAdsService: ResponseAdsService) {

    @PostMapping("create/{adsId}")
    fun createResponseAds (@PathVariable("adsId") adsId: Long) : Mono<Long>{
        return responseAdsService.createResponseAds(adsId)
    }

    @GetMapping("find_all")
    fun findAds () : Flux<AdsResponseDto>{
        return responseAdsService.getResponseForAds()
    }
}