package ru.era.lab_account.controller

import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import ru.era.lab_account.dto.mark.CreateMarkDto
import ru.era.lab_account.dto.mark.MarkDto
import ru.era.lab_account.services.crud_service.mark.MarkService

@RestController
@RequestMapping("mark")
class MarkController (private val markService: MarkService) {

    @PostMapping("create")
    //@PreAuthorize("hasRole('STAKEHOLDER')")
    fun setMark(@RequestBody dto: CreateMarkDto): Mono<Long> {
        return markService.createMark(dto)
    }

    @GetMapping("getByProject/{projectId}")
    fun getMarkByProject(@PathVariable("projectId") projectId: Long): Flux<MarkDto> {
        return markService.markForProject(projectId)
    }

}