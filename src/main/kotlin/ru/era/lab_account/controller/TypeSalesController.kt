package ru.era.lab_account.controller

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Flux
import ru.era.lab_account.dto.helper.KeyValueDto
import ru.era.lab_account.services.crud_service.type_sales.TypeSalesService

@RestController
@RequestMapping("types_sales")
class TypeSalesController(private val typeSalesService: TypeSalesService) {

    @GetMapping("find_all")
    fun findAllTypeSales () : Flux<KeyValueDto>{
        return typeSalesService.findAll()
    }
}