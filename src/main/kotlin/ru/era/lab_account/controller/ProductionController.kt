package ru.era.lab_account.controller

import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Mono
import ru.era.lab_account.dto.production.CreateProductionDto
import ru.era.lab_account.services.crud_service.production.ProductionService

@RestController
@RequestMapping("productions")
class ProductionController (private val productionService: ProductionService) {

    @PostMapping("create")
    fun createProduction (@RequestBody dto: CreateProductionDto) : Mono<Long> {
        return productionService.createProduction(dto)
    }
}