package ru.era.lab_account.controller

import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Flux
import ru.era.lab_account.dto.helper.KeyValueDto
import ru.era.lab_account.services.crud_service.company.CompanyService

@RestController
@RequestMapping("company")
class CompanyController(private val companyService: CompanyService) {

    @GetMapping("find_all",produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getAllCompany () : Flux<KeyValueDto>{
        return companyService.getAllCompanies()
    }
}