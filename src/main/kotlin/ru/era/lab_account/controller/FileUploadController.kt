package ru.era.lab_account.controller


import org.springframework.http.MediaType
import org.springframework.http.codec.multipart.FilePart
import org.springframework.http.server.reactive.ServerHttpRequest
import org.springframework.http.server.reactive.ServerHttpResponse
import org.springframework.web.bind.annotation.*
import reactor.core.publisher.Mono
import ru.era.lab_account.services.crud_service.file.FileEntityService
import ru.era.lab_account.services.file.FileService


@RestController
@RequestMapping("file")
class FileUploadController (private val fileEntityService: FileEntityService) {

    @PostMapping("upload", consumes = [MediaType.MULTIPART_FORM_DATA_VALUE], produces = [MediaType.APPLICATION_JSON_VALUE])
    fun uploadFileTest (@RequestPart("file") filePart: Mono<FilePart>) : Mono<Long> {
        return fileEntityService.saveFile(filePart)
    }

    /*@PostMapping("download/{file_id}")
    fun downloadFile (@PathVariable("file_id", required = true) fileId: Long,
                      response: ServerHttpResponse) : Mono<Void>{
        return
    }*/
}