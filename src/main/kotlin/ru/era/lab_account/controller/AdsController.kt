package ru.era.lab_account.controller

import org.springframework.web.bind.annotation.*
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import ru.era.lab_account.dto.ads.AdsTableDto
import ru.era.lab_account.dto.ads.AdsTableDtoToUser
import ru.era.lab_account.dto.ads.CreateAdsDto
import ru.era.lab_account.models.pagination.PaginationRequest
import ru.era.lab_account.services.crud_service.ads_table.AdsTableService

@RestController
@RequestMapping("ads")
class AdsController (private val adsTableService: AdsTableService) {

    @PostMapping("create")
    fun createAds (@RequestBody dto: CreateAdsDto) : Mono<Long>{
        return adsTableService.createAds(dto)
    }

    @PostMapping("delete/{adId}")
    fun deleteAd (@PathVariable("adId") adId: Long) : Mono<Long>{
        return adsTableService.deleteAds(adId)
    }

    @GetMapping("find_all")
    fun findAllAds (@RequestParam("limit") limit: Int,
                    @RequestParam("offset") offset: Int) : Flux<AdsTableDto>{
        return adsTableService.getAllAds(PaginationRequest(limit,offset))
    }

    @GetMapping("find_user")
    fun findAdsForUser (@RequestParam("limit") limit: Int,
                        @RequestParam("offset") offset: Int) : Flux<AdsTableDto> {
        return adsTableService.getAdsByUser(PaginationRequest(limit,offset))
    }
}