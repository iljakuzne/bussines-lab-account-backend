package ru.era.lab_account.controller

import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Flux
import ru.era.lab_account.dto.helper.KeyValueDto
import ru.era.lab_account.services.crud_service.scope.ScopeService

@RestController
@RequestMapping("scope")
class ScopeController (private val scopeService: ScopeService) {

    @GetMapping("find_all",produces = [MediaType.APPLICATION_JSON_VALUE])
    fun findAllScope () : Flux<KeyValueDto> {
        return scopeService.findAll()
    }
}