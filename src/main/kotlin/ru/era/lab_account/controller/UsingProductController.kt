package ru.era.lab_account.controller

import org.springframework.http.MediaType.APPLICATION_JSON_VALUE
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Flux
import ru.era.lab_account.dto.helper.KeyValueDto
import ru.era.lab_account.services.crud_service.use_product.UsingProductService

@RestController
@RequestMapping("using_product")
class UsingProductController(private val usingProductService: UsingProductService) {

    @GetMapping("find_all", produces = [APPLICATION_JSON_VALUE])
    fun getUsingProduct () : Flux<KeyValueDto>{
        return usingProductService.findAllUsingProduct()
    }
}