package ru.era.lab_account.controller

import org.springframework.http.MediaType.APPLICATION_JSON_VALUE
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Flux
import ru.era.lab_account.dto.helper.KeyValueDto
import ru.era.lab_account.services.crud_service.region.RegionService

@RestController
@RequestMapping("region")
class RegionController (private val regionService: RegionService) {

    @GetMapping("find_all", produces = [APPLICATION_JSON_VALUE])
    fun findAll () : Flux<KeyValueDto>{
        return regionService.findAllRegions()
    }
}