package ru.era.lab_account.controller


import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseCookie
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Mono
import ru.era.lab_account.consts.WebApplicationConsts
import ru.era.lab_account.dto.auth.AuthenticationDto
import ru.era.lab_account.dto.auth.RegistrationDto
import ru.era.lab_account.dto.user.UserDto
import ru.era.lab_account.dto.user.UserDtoWithToken
import ru.era.lab_account.mapper.BussinesUserToUserDto
import ru.era.lab_account.services.login.AuthenticationService
import ru.era.lab_account.services.registration.RegistrationService

@RestController
@RequestMapping("auth")
class AuthorizationController(private val authenticationService: AuthenticationService,
                              private val registrationService: RegistrationService) {

    @PostMapping("login", produces = [MediaType.APPLICATION_JSON_VALUE])
    fun login (@RequestBody authenticationDto: AuthenticationDto) : Mono<ResponseEntity<UserDtoWithToken>> {
        return authenticationService.authUser(authenticationDto)
            .map {model->
                     ResponseEntity.ok(UserDtoWithToken(model.bussinessUserDto,model.jwtToken))
                    //.header(WebApplicationConsts.authorisationHeader, model.jwtToken)

            }
    }

    @PostMapping("register", produces = [MediaType.APPLICATION_JSON_VALUE])
    fun registration (@RequestBody dto: RegistrationDto) : Mono<Long>{
        return registrationService.createNewUser(dto)
    }

}