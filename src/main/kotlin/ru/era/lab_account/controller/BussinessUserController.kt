package ru.era.lab_account.controller

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Flux
import ru.era.lab_account.dto.user.UserDto
import ru.era.lab_account.services.crud_service.bussiness_user.BussinesUserService
import javax.annotation.security.RolesAllowed

@RestController
@RequestMapping("user")
class BussinessUserController (private val bussinesUserService: BussinesUserService) {

    @GetMapping("find_all")
    @RolesAllowed("ADMIN")
    fun getUsers () : Flux<UserDto>{
        return bussinesUserService.findAllUsers()
    }
}