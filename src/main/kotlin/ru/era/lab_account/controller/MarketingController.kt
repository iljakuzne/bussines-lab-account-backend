package ru.era.lab_account.controller

import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Mono
import ru.era.lab_account.dto.marketing.CreateMarketingDto
import ru.era.lab_account.services.crud_service.marketing.MarketingService

@RestController
@RequestMapping("marketing")
class MarketingController(private val marketingService: MarketingService) {

    @PostMapping("create")
    fun createMarketing (@RequestBody dto: CreateMarketingDto) : Mono<Long> {
        return marketingService.createMarketing(dto)
    }
}