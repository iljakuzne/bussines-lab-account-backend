package ru.era.lab_account.controller

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Flux
import ru.era.lab_account.dto.helper.KeyValueDto
import ru.era.lab_account.services.crud_service.criteria_mark.CriteriaMarkService

@RestController
@RequestMapping("criteria_mark")
class CriteriaMarkController(private val criteriaMarkService: CriteriaMarkService) {

    @GetMapping("findByStatus/{status}")
    fun getCriteriaMarkByStatus (@PathVariable("status") status: Int) : Flux<KeyValueDto>{
        return criteriaMarkService.getCriteriaByStatus(status)
    }
}