package ru.era.lab_account

import org.springframework.boot.WebApplicationType
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.builder.SpringApplicationBuilder

@SpringBootApplication()
class BussinessLabAccountApplication

fun main (args : Array<String>){
    SpringApplicationBuilder(BussinessLabAccountApplication::class.java)
        .web(WebApplicationType.REACTIVE)
        .run(*args)
}