package ru.era.lab_account.domain

import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table

@Table("project_file")
data class FileProject(@get:Id @get:Column("id") val id: Long? = null,
                       @get:Column("file_name") val fileName: String,
                       @get:Column("hash") val hash: String? = null,
                       @get:Column("content_type") val contentType: String,
                       @get:Column("path") val path: String?=null)