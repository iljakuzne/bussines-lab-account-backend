package ru.era.lab_account.domain

import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table
import java.time.Instant

@Table("market")
data class MarketTemplate (@get:Id @get:Column("id") val id : Long? = null,
                           @get:Column("region_id") val regionId: Long,
                           @get:Column("project_id") val projectId: Long,
                           @get:Column("barrier_entry") val barrierEntry: String,
                           @get:Column("output_analiz") val outputAnaliz: String)