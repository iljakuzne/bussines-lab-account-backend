package ru.era.lab_account.domain

import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table

@Table("ways_advertising_to_marketing")
data class WaysAdvertismentToMarketing (@get:Id @get:Column("id") val id: Long? = null,
                                        @get:Column("marketing_id") val marketingId: Long,
                                        @get:Column("ways_advertising_id")  val waysAdvertisingId: Long)