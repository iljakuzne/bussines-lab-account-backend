package ru.era.lab_account.domain

import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table

@Table("competitors")
data class Competitor (@get:Id @get:Column("id") val id: Long? = null,
                       @get:Column("market_id") val marketId: Long,
                       @get:Column("name") val competitorName: String,
                       @get:Column("advantages") val advantages: String,
                       @get:Column("problem_solved") val problemSolved: String)