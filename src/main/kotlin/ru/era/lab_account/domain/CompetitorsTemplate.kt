package ru.era.lab_account.domain

import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table
import java.time.Instant

@Table("competitors")
data class CompetitorsTemplate (@get:Id @get:Column("id") val id : Long? = null,
                                @get:Column("name") val competitorsName: String,
                                @get:Column("advantages") val competitorsAdvantages: String?,
                                @get:Column("problem_solved") val problemSolved: String?)