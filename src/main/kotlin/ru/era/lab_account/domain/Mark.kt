package ru.era.lab_account.domain

import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table

@Table("mark")
data class Mark (@get:Id @get:Column("id") val id: Long? = null,
                 @get:Column("user_id") val userId: Long,
                 @get:Column("mark_criteria") val markCriteria: Long,
                 @get:Column("mark") val mark: Int,
                 @get:Column("project_id") val projectId: Long)