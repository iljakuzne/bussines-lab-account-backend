package ru.era.lab_account.domain

import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table
import java.time.Instant

@Table("ads")
data class AdsTable (@get:Id @get:Column("id") val id : Long? = null,
                     @get:Column("name") val name: String,
                     @get:Column("content") val content: String,
                     @get:Column("created_at") val created: Instant,
                     @get:Column("user_id") val userId: Long)
