package ru.era.lab_account.domain

import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table
import ru.era.lab_account.models.project.ProjectStatus

@Table("mark_criteria")
data class MarkCriteria (@get:Id @get:Column("id") val id: Long,
                         @get:Column("criteria_name") val criteriaName: String,
                         @get:Column("criteria_status")val criteriaStatus: ProjectStatus)