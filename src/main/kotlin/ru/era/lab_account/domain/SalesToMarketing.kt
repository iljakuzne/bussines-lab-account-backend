package ru.era.lab_account.domain

import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table

@Table("type_sales_to_marketing")
data class SalesToMarketing (@get:Id @get:Column("id") val id: Long? = null,
                             @get:Column("marketing_id ") val marketingId: Long,
                             @get:Column("type_sales_id ") val typeSales: Long)