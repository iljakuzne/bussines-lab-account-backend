package ru.era.lab_account.domain

import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table

@Table("equipment")
data class PurchaseEquipment (@get:Id @get:Column("id") val id: Long?=null,
                              @get:Column("production_id") val productionId: Long,
                              @get:Column("name") val name: String,
                              @get:Column("cost")val cost: Double?)