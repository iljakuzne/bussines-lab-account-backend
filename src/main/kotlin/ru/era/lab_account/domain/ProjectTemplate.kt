package ru.era.lab_account.domain

import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table
import ru.era.lab_account.models.project.ProjectStatus
import java.time.Instant

@Table("project")
data class ProjectTemplate (@get:Id @get:Column("id") val id : Long? = null,
                            @get:Column("user_id") val userId: Long,
                            @get:Column("name") val projectName: String,
                            @get:Column("target") val projectTarget: String,
                            @get:Column("problem_solved") val problemSolved: String?,
                            @get:Column("advantages") val advantages: String?,
                            @get:Column("scope_id") val scopeId: Long,
                            @get:Column("stage_realization_id") val stageRealizationId: Long,
                            @get:Column("status") val status: ProjectStatus,
                            @get:Column("implement") val implementData: Int,
                            @get:Column("create_date") val createDate: Instant)