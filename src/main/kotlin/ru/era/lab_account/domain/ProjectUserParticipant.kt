package ru.era.lab_account.domain

import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table
import ru.era.lab_account.models.user.UserRole

@Table("project_user_participant")
data class ProjectUserParticipant (@get:Id @get:Column("id") val id : Long,
                                   @get:Column("user_id") val userId: Long,
                                   @get:Column("role") val role: UserRole)