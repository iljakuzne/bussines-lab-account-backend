package ru.era.lab_account.domain

import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table

@Table("response_ads")
data class ResponseAds (@get:Id @get:Column("id") val id : Long? = null,
                        @get:Column("user_id") val userId: Long,
                        @get:Column("ads_id") val adsId: Long)