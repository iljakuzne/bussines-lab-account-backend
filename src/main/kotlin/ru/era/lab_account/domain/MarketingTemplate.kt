package ru.era.lab_account.domain

import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table

@Table("marketing")
data class MarketingTemplate (@get:Id @get:Column("id") val id : Long? = null,
                              @get:Column("project_id") val projectId: Long,
                              @get:Column("cost") val marketingProductCost: Double,
                              @get:Column("volume_sales") val volumeSales: Double,
                              @get:Column("market_cost") val marketingCost: Double)