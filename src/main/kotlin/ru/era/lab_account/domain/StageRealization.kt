package ru.era.lab_account.domain

import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table

@Table("stage_realization")
data class StageRealization(@get:Id @get:Column("id") val id: Long?,
                            @get:Column("stage_realization") val stageRealization: String)