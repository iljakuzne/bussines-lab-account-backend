package ru.era.lab_account.domain

import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table

@Table("product")
data class ProductTemplate (@get:Id @get:Column("id") val id : Long? = null,
                            @get:Column("project_id") val projectId: Long,
                            @get:Column("name") val productName: String,
                            @get:Column("functional") val productFunctional: String,
                            @get:Column("characteristics") val productCharacteristics: String?,
                            @get:Column("using_product_id") val usingProductId: Long,
                            @get:Column("advantages") val productAdvantages: String,
                            @get:Column("additional_information") val additionalInformation: String?)