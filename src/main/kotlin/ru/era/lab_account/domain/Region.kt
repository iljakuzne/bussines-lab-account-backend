package ru.era.lab_account.domain

import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table

@Table("region")
data class Region (@get:Id @get:Column("id") val id: Long?,
                   @get:Column("name") val name: String)