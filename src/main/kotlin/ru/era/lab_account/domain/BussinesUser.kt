package ru.era.lab_account.domain

import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table
import ru.era.lab_account.models.user.UserRole

@Table("bussiness_user")
data class BussinesUser(@get:Id @get:Column("id") val id : Long? = null,
                        @get:Column("login") val login: String,
                        @get:Column("first_name") val firstName: String?=null,
                        @get:Column("last_name") val lastName: String?=null,
                        @get:Column("second_name") val secondName: String?=null,
                        @get:Column("phone") val phone: String?,
                        @get:Column("email") val email: String,
                        @get:Column("password") val password: String,
                        @get:Column("salt") val salt: String,
                        @get:Column("role") val role: UserRole)