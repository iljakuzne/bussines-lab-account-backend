package ru.era.lab_account.domain

import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table
import ru.era.lab_account.models.project.ProjectStatus

@Table("production")
data class ProductionTemplate (@get:Id @get:Column("id") val id : Long? = null,
                               @get:Column("project_id") val projectId: Long,
                               @get:Column("cost_prise") val costPrise: Double,
                               @get:Column("time_production") val timeProduction: Double)