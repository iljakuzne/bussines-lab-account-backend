package ru.era.lab_account.domain

import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table

@Table("provider")
data class PurchaseProvider (@get:Id @get:Column("id") val id: Long? = null,
                             @get:Column("production_id") val productionId: Long,
                             @get:Column("name") val name : String,
                             @get:Column("resources") val resources: Double?)