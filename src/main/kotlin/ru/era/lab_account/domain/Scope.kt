package ru.era.lab_account.domain

import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table

@Table("scope")
data class Scope (@get:Id @get:Column("id") val id: Long?,
                  @get:Column("name") val name: String)