package ru.era.lab_account.domain.view

import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table

@Table
data class MarkResultView (@get:Id @get:Column("id") val id: Long,
                           @get:Column("criteria_name") val criteriaName: String,
                           @get:Column("summa") val summa: Int)