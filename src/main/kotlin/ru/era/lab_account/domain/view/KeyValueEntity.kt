package ru.era.lab_account.domain.view

import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table

@Table
data class KeyValueEntity (@get:Id @get:Column("id") val id: Long,
                           @get:Column("value") val value: String)