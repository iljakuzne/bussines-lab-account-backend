package ru.era.lab_account.domain.view

import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table
import ru.era.lab_account.models.project.ProjectStatus
import java.time.Instant

@Table
data class ProjectCardRating (@get:Id @Column("id") val id: Long,
                              @Column("user_id") val userId: Long,
                              @Column("user_name") val userName: String,
                              @Column("user_email") val userEmail: String?,
                              @Column("user_phone") val userPhone: String?,
                              @Column("project_name") val projectName: String,
                              @Column("project_target") val projectTarget: String,
                              @Column("advantages") val advantages: String?,
                              @Column("problem_solved") val problemSolved: String?,
                              @Column("status") val status: ProjectStatus,
                              @Column("create_date") val createDate: Instant,
                              @Column("rating") val rating: Int)