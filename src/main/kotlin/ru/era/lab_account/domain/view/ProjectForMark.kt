package ru.era.lab_account.domain.view

import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table
import ru.era.lab_account.models.project.ProjectStatus

@Table
data class ProjectForMark (@get:Id @get:Column("id") val projectId : Long,
                           @get:Column("user_id") val userId: Long,
                           @get:Column("status") val status: ProjectStatus)