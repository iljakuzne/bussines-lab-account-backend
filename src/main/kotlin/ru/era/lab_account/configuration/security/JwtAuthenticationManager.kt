package ru.era.lab_account.configuration.security

import org.springframework.security.authentication.ReactiveAuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.stereotype.Component
import reactor.core.publisher.Mono
import reactor.core.publisher.SynchronousSink


@Component
class JwtAuthenticationManager (private val jwtUtil: JwtUtils) : ReactiveAuthenticationManager {

    override fun authenticate(authentication: Authentication): Mono<Authentication> {
        return Mono.fromCallable {
            authentication.credentials.toString()
        }
            .handle { credentials, synchronousSink: SynchronousSink<Authentication> ->
                if (!jwtUtil.validateToken(credentials)){
                    synchronousSink.error(IllegalArgumentException("Не авторизован"))
                } else {
                    val claims = jwtUtil.getAllClaimsFromToken(credentials)
                    synchronousSink.next(
                        UsernamePasswordAuthenticationToken(claims, null, mutableListOf(SimpleGrantedAuthority(claims.get("role",String::class.java))))
                    )
                }
            }
    }
}