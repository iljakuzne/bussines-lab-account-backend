package ru.era.lab_account.configuration.security

import org.springframework.security.authentication.ReactiveAuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContext
import org.springframework.security.core.context.SecurityContextImpl
import org.springframework.security.web.server.context.ServerSecurityContextRepository
import org.springframework.stereotype.Component
import org.springframework.web.server.ServerWebExchange
import reactor.core.publisher.Mono
import reactor.core.publisher.SynchronousSink
import ru.era.lab_account.consts.WebApplicationConsts


@Component
class SecurityContextRepository (private val authenticationManager: ReactiveAuthenticationManager) : ServerSecurityContextRepository {

    override fun save(swe: ServerWebExchange, sc: SecurityContext): Mono<Void> {
        throw UnsupportedOperationException("Not supported yet.")
    }

    override fun load(swe: ServerWebExchange): Mono<SecurityContext> {
        return Mono.fromCallable {
            swe.request.getHeaders().getFirst(WebApplicationConsts.authorisationHeader)
        }
            .switchIfEmpty(Mono.error(IllegalArgumentException("Отсутвтует токен авторизации")))
            .handle{token, synchronousSink: SynchronousSink<Authentication>->
                if (!token.isNullOrEmpty()){
                    synchronousSink.next(UsernamePasswordAuthenticationToken(token, token))
                }
            }
            .flatMap(authenticationManager::authenticate)
            .map{manager->SecurityContextImpl(manager)}
    }
}