package ru.era.lab_account.configuration.mapper

import org.modelmapper.ModelMapper
import org.modelmapper.convention.MatchingStrategies
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import ru.era.lab_account.mapper.MapperService


@Configuration
class MapperConfiguration {

    @Bean
    fun modelMapper(mapperServices: List<MapperService<*,*>>): ModelMapper {
        val mapper = ModelMapper()
        mapper.configuration
            .setMatchingStrategy(MatchingStrategies.STRICT)
            .setFieldMatchingEnabled(true)
            .setSkipNullEnabled(true)
            .fieldAccessLevel = org.modelmapper.config.Configuration.AccessLevel.PRIVATE

        mapperServices.forEach {service->
            mapper.createTypeMap(service.sourceClass(),service.destinationClass())
                .converter = service.getConverter()
        }

        return mapper
    }
}