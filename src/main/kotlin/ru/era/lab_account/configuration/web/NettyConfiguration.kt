package ru.era.lab_account.configuration.web

import org.springframework.boot.web.embedded.netty.NettyReactiveWebServerFactory
import org.springframework.boot.web.server.WebServer
import org.springframework.context.annotation.Configuration
import org.springframework.http.server.reactive.ContextPathCompositeHandler
import org.springframework.http.server.reactive.HttpHandler
import java.util.*


@Configuration
class NettyConfiguration : NettyReactiveWebServerFactory() {

    override fun getWebServer(httpHandler: HttpHandler): WebServer {
        val handlerMap: MutableMap<String, HttpHandler> = HashMap()
        handlerMap["/api"] = httpHandler
        return super.getWebServer(ContextPathCompositeHandler(handlerMap))
    }
}