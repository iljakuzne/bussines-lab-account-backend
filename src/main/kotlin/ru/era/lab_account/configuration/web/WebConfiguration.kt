package ru.era.lab_account.configuration.web

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.KotlinModule
import org.springframework.boot.web.embedded.netty.NettyReactiveWebServerFactory
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.reactive.config.CorsRegistry
import org.springframework.web.reactive.config.EnableWebFlux
import org.springframework.web.reactive.config.WebFluxConfigurer

@Configuration
@EnableWebFlux
class WebConfiguration: WebFluxConfigurer
{

    override fun addCorsMappings(registry: CorsRegistry)
    {
        registry.addMapping("/**")
            .allowedOrigins("*") // any host or put domain(s) here
            .allowedMethods("*") // put the http verbs you want allow
            .allowedHeaders("*") // put the http headers you want allow
    }

    @Bean
    fun objectMapper () : ObjectMapper{
        return ObjectMapper()
            .registerModule(JavaTimeModule())
            .registerModule(KotlinModule())
    }
}