package ru.era.lab_account.configuration

import io.r2dbc.pool.ConnectionPool
import io.r2dbc.pool.ConnectionPoolConfiguration
import io.r2dbc.postgresql.PostgresqlConnectionConfiguration
import io.r2dbc.postgresql.PostgresqlConnectionFactory
import io.r2dbc.spi.ConnectionFactory
import org.flywaydb.core.Flyway
import org.postgresql.ds.PGSimpleDataSource
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.flyway.FlywayMigrationInitializer
import org.springframework.boot.autoconfigure.flyway.FlywayMigrationStrategy
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.data.r2dbc.config.AbstractR2dbcConfiguration
import org.springframework.data.r2dbc.repository.config.EnableR2dbcRepositories
import org.springframework.r2dbc.connection.R2dbcTransactionManager
import org.springframework.transaction.ReactiveTransactionManager
import org.springframework.transaction.reactive.TransactionalOperator
import java.time.Duration
import javax.sql.DataSource


@Configuration
@EnableR2dbcRepositories(basePackages=["ru.era.lab_account.dao"])
class DaoConfiguration (@Value("\${spring.database.host}") private val host: String,
                        @Value("\${spring.database.port}") private val port: Int,
                        @Value("\${spring.database.database}") private val database: String,
                        @Value("\${spring.database.username}") private val username: String,
                        @Value("\${spring.database.password}") private val password: String,
                        @Value("\${spring.database.schema}") private val schema: String,
                        @Value("\${spring.database.pool.maxSize}") private val maxSize: Int,
                        @Value("\${spring.database.pool.initSize}") private val initialSize: Int,
                        @Value("\${spring.database.pool.maxIdleTime}") private val maxIdleTime : Long,
                        @Value("\${spring.database.pool.maxLifeTime}") private val maxLifeTime : Long,
                        @Value("\${spring.database.migration.path}")private val migrationPath: String) :
    AbstractR2dbcConfiguration() {

    @Bean
    override fun connectionFactory(): ConnectionFactory {
        val postgresqlConfiguration = PostgresqlConnectionFactory(
            PostgresqlConnectionConfiguration.builder()
                .host(host)
                .port(port)
                .username(username)
                .password(password)
                .database(database)
                .schema(schema)
                .build()
        )
        return ConnectionPool(ConnectionPoolConfiguration.builder(postgresqlConfiguration)
            .maxIdleTime(Duration.ofSeconds(maxIdleTime))
            .maxLifeTime(Duration.ofSeconds(maxLifeTime))
            .initialSize(initialSize)
            .maxSize(maxSize)
            .build())
    }

    @Bean
    fun connectionFactoryTransactionManager(connectionFactory: ConnectionPool): ReactiveTransactionManager {
        return R2dbcTransactionManager(connectionFactory)
    }

    @Bean
    fun transactionalOperator (transactionManager: ReactiveTransactionManager) : TransactionalOperator {
        return TransactionalOperator.create(transactionManager)
    }


    @Bean
    fun dataSource (): DataSource {
        val dataSource = PGSimpleDataSource()
        dataSource.user = username
        dataSource.password = password
        dataSource.serverNames = arrayOf(host)
        dataSource.databaseName = database
        dataSource.portNumbers = IntArray(port)
        dataSource.currentSchema = schema
        return dataSource
    }

    @Bean
    fun springFlyway (dataSource: DataSource): Flyway {
        return Flyway.configure()
            .dataSource(dataSource)
            .locations(migrationPath)
            .baselineOnMigrate(true)
            .load()
    }

    @Bean
    fun flywayMigrationInitializer (flyway: Flyway): FlywayMigrationInitializer {
        return FlywayMigrationInitializer(flyway)
    }



}