package ru.era.lab_account.models.market

import ru.era.lab_account.domain.MarketTemplate

data class MarketWithCompetitorsNameModel (val marketTemplate: MarketTemplate,
                                           val competitorsName: String)