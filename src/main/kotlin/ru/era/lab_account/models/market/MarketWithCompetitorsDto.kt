package ru.era.lab_account.models.market

import ru.era.lab_account.domain.MarketTemplate
import ru.era.lab_account.dto.competitor.CreateCompetitorDto

data class MarketWithCompetitorsDto (val market: MarketTemplate,
                                     val competitors: List<CreateCompetitorDto>)