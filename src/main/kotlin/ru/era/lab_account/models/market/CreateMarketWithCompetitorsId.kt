package ru.era.lab_account.models.market

import ru.era.lab_account.domain.MarketTemplate
import ru.era.lab_account.dto.competitors.CreateCompetitorsDto

data class CreateMarketWithCompetitorsId (val competitorsId: Long,
                                          val marketTemplate: MarketTemplate)