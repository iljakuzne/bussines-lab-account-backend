package ru.era.lab_account.models.marketing

import ru.era.lab_account.domain.MarketingTemplate
import ru.era.lab_account.dto.product.ProductCardDto
import ru.era.lab_account.dto.project.CreateProjectDto

data class CreateMarketing (val marketingTemplate: MarketingTemplate)