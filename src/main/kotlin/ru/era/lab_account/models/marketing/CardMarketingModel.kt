package ru.era.lab_account.models.marketing

import ru.era.lab_account.domain.MarketingTemplate

data class CardMarketingModel (val templateMarketing: MarketingTemplate,
                               val productName: String)