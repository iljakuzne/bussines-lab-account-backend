package ru.era.lab_account.models.user

import java.time.Instant

data class UserModel (val userId: Long,
                      val login: String,
                      val userRole: UserRole,
                      val created: Instant,
                      val timeLeaf: Int)