package ru.era.lab_account.models.user

enum class UserRole {
    USER,  // инициатор
    ASSISTANT, // помощник
    ADMIN, // администратор
    CUSTOMER, // заказчик
    CURATOR, // заказчик
    STAKEHOLDER; // стейкхолдер
}