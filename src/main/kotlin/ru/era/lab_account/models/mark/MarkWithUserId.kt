package ru.era.lab_account.models.mark

import ru.era.lab_account.dto.mark.CreateMarkDto

data class MarkWithUserId (val createMarkDto: CreateMarkDto,
                           val userId: Long)