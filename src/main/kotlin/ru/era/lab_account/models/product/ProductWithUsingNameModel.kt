package ru.era.lab_account.models.product

import ru.era.lab_account.domain.ProductTemplate

data class ProductWithUsingNameModel (val productTemplate: ProductTemplate,
                                      val usingProductName: String)