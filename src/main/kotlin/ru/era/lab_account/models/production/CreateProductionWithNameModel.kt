package ru.era.lab_account.models.production

import ru.era.lab_account.domain.ProductionTemplate

data class CreateProductionWithNameModel (val production : ProductionTemplate,
                                          val projectName : String)