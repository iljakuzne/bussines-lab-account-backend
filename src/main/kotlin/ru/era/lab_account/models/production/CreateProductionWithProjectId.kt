package ru.era.lab_account.models.production

import ru.era.lab_account.domain.ProductionTemplate

data class CreateProductionWithProjectId (val production : ProductionTemplate,
                                          val projectId : Long)