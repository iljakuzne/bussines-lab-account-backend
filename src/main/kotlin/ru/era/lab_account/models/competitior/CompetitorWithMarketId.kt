package ru.era.lab_account.models.competitior

import ru.era.lab_account.dto.competitor.CreateCompetitorDto

data class CompetitorWithMarketId (val competitor : CreateCompetitorDto,
                                   val marketId: Long)