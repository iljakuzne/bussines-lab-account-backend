package ru.era.lab_account.models.ads

import ru.era.lab_account.dto.ads.CreateAdsDto

data class AdsDtoWithUserIdModel (val userId: Long,
                                  val createAdsDto: CreateAdsDto)