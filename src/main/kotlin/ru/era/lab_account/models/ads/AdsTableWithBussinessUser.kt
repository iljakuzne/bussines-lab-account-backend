package ru.era.lab_account.models.ads

import ru.era.lab_account.domain.AdsTable
import ru.era.lab_account.domain.BussinesUser

data class AdsTableWithBussinessUser (val user: BussinesUser,
                                      val adsTable: AdsTable)