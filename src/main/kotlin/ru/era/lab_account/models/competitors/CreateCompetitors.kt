package ru.era.lab_account.models.competitors

import ru.era.lab_account.domain.CompetitorsTemplate

data class CreateCompetitors (val competitorsTemplate: CompetitorsTemplate)