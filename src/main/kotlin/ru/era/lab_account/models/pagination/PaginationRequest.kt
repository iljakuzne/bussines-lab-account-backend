package ru.era.lab_account.models.pagination

data class PaginationRequest (val limit: Int,
                              val pageCount: Int)