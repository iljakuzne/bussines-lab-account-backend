package ru.era.lab_account.models.project

import ru.era.lab_account.domain.BussinesUser
import ru.era.lab_account.domain.ProjectTemplate

data class CardProjectModel (val templateProject: ProjectTemplate,
                             val scopeName: String,
                             val bussinessUser: BussinesUser)