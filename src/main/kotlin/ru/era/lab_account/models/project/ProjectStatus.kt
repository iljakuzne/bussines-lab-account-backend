package ru.era.lab_account.models.project

enum class ProjectStatus(val number: Int, val sumPointNext: Int) {
    RESUME(1, 1000),
    DESCRIPTION(2, 800),
    MARKET(3, 600),
    FINANCE(4, Int.MAX_VALUE);

    companion object {

        fun findByNumber (status: Int) : ProjectStatus? {
            for (l in ProjectStatus.values()){
                if (l.number == status){
                    return l
                }
            }
            return null
        }

        fun checkNextStatus (earnPoint: Int, currentStatus: ProjectStatus) : Boolean = currentStatus.sumPointNext <= earnPoint

        fun getNextStatus (currentStatus: ProjectStatus) : ProjectStatus {
            return when(currentStatus){
                RESUME -> DESCRIPTION
                DESCRIPTION -> MARKET
                MARKET -> FINANCE
                FINANCE -> FINANCE
            }
        }
    }
}