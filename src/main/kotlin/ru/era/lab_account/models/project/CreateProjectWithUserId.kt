package ru.era.lab_account.models.project

import ru.era.lab_account.dto.project.CreateProjectDto

data class CreateProjectWithUserId (val project : CreateProjectDto,
                                    val userId : Long)