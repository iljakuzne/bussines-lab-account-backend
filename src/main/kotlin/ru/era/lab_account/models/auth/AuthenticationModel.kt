package ru.era.lab_account.models.auth

import ru.era.lab_account.dto.user.UserDto
import ru.era.lab_account.models.user.UserModel

data class AuthenticationModel(val jwtToken: String,
                               val bussinessUserDto: UserDto,
                               val userModel: UserModel)