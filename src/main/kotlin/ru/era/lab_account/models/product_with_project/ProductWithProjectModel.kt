package ru.era.lab_account.models.product_with_project

import ru.era.lab_account.domain.BussinesUser
import ru.era.lab_account.domain.ProductTemplate
import ru.era.lab_account.domain.ProjectTemplate

data class ProductWithProjectModel (val projectTemplate: ProjectTemplate,
                                    val productTemplate: ProductTemplate,
                                    val bussinesUser: BussinesUser)