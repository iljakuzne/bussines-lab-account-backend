package ru.era.lab_account.mapper

import org.modelmapper.AbstractConverter
import org.springframework.stereotype.Service
import ru.era.lab_account.domain.MarketTemplate
import ru.era.lab_account.dto.market.CreateMarketDto

@Service
class CreateMarketDtoToMarketTemplate :MapperService<CreateMarketDto, MarketTemplate> {
    override fun getConverter(): AbstractConverter<CreateMarketDto, MarketTemplate> {
        return object : AbstractConverter<CreateMarketDto, MarketTemplate> () {

            override fun convert(source: CreateMarketDto): MarketTemplate {
                return MarketTemplate(projectId = source.projectId,
                    barrierEntry = source.barrierEntry,
                    outputAnaliz = source.outputAnaliz,
                    regionId = source.regionId)
            }

        }
    }

    override fun sourceClass(): Class<CreateMarketDto> {
        return CreateMarketDto::class.java
    }

    override fun destinationClass(): Class<MarketTemplate> {
        return MarketTemplate::class.java
    }
}