package ru.era.lab_account.mapper

import org.modelmapper.AbstractConverter
import org.springframework.stereotype.Service
import ru.era.lab_account.domain.BussinesUser
import ru.era.lab_account.dto.auth.RegistrationDto
import ru.era.lab_account.models.user.UserRole
import ru.era.lab_account.services.login.HashService
import java.util.*
import kotlin.math.log

@Service
class RegistrationDtoToBussinesUser(private val hashService: HashService) : MapperService<RegistrationDto, BussinesUser> {

    override fun getConverter(): AbstractConverter<RegistrationDto, BussinesUser> {

        return object : AbstractConverter<RegistrationDto, BussinesUser>() {

            override fun convert(source: RegistrationDto): BussinesUser {
                val salt = UUID.randomUUID().toString()
                val saltPassword = hashService.generateHash(source.password,salt)
                return BussinesUser(login = source.login,
                    phone = source.phone,
                    email = source.email,
                    password = saltPassword,
                    salt = salt,
                    role = UserRole.USER)
            }

        }
    }

    override fun sourceClass(): Class<RegistrationDto> {
        return RegistrationDto::class.java
    }

    override fun destinationClass(): Class<BussinesUser> {
       return BussinesUser::class.java
    }
}