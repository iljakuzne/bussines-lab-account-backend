package ru.era.lab_account.mapper

import org.modelmapper.AbstractConverter
import org.springframework.stereotype.Service
import ru.era.lab_account.domain.TypeSales
import ru.era.lab_account.dto.helper.KeyValueDto

@Service
class TypeSalesToKeyValueDto : MapperService<TypeSales, KeyValueDto> {
    override fun getConverter(): AbstractConverter<TypeSales, KeyValueDto> {
        return object : AbstractConverter<TypeSales, KeyValueDto> (){

            override fun convert(source: TypeSales): KeyValueDto {
                return KeyValueDto(id = source.id!!,
                    value = source.name)
            }

        }
    }

    override fun sourceClass(): Class<TypeSales> {
        return TypeSales::class.java
    }

    override fun destinationClass(): Class<KeyValueDto> {
        return KeyValueDto::class.java
    }
}