package ru.era.lab_account.mapper

import org.modelmapper.AbstractConverter
import org.springframework.stereotype.Service
import ru.era.lab_account.domain.AdsTable
import ru.era.lab_account.dto.ads.AdsTableDtoToUser

@Service
class AdsTableToAdsTableDtoToUser : MapperService<AdsTable, AdsTableDtoToUser> {

    override fun getConverter(): AbstractConverter<AdsTable, AdsTableDtoToUser> {
        return object : AbstractConverter<AdsTable, AdsTableDtoToUser>(){
            override fun convert(source: AdsTable): AdsTableDtoToUser {
                return AdsTableDtoToUser(id = source.id!!,
                    name = source.name,
                    content = source.content,
                    created = source.created)
            }

        }
    }

    override fun sourceClass(): Class<AdsTable> {
        return AdsTable::class.java
    }

    override fun destinationClass(): Class<AdsTableDtoToUser> {
       return AdsTableDtoToUser::class.java
    }
}