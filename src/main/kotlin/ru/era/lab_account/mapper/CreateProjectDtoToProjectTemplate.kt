package ru.era.lab_account.mapper

import org.modelmapper.AbstractConverter
import org.springframework.stereotype.Service
import ru.era.lab_account.domain.ProjectTemplate
import ru.era.lab_account.models.project.CreateProjectWithUserId
import ru.era.lab_account.models.project.ProjectStatus
import java.time.Instant

@Service
class CreateProjectDtoToProjectTemplate : MapperService<CreateProjectWithUserId, ProjectTemplate> {

    override fun getConverter(): AbstractConverter<CreateProjectWithUserId, ProjectTemplate> {
        return object : AbstractConverter<CreateProjectWithUserId, ProjectTemplate>(){

            override fun convert(source: CreateProjectWithUserId): ProjectTemplate {
                return ProjectTemplate(userId = source.userId,
                projectName = source.project.projectName,
                problemSolved = source.project.solvedProblem,
                projectTarget = source.project.projectTarget,
                advantages = source.project.advantages,
                scopeId = source.project.scopeId,
                stageRealizationId = source.project.stageId,
                implementData = source.project.readyTime,
                status = ProjectStatus.RESUME,
                createDate = Instant.now())
            }
        }
    }

    override fun sourceClass(): Class<CreateProjectWithUserId> {
        return CreateProjectWithUserId::class.java
    }

    override fun destinationClass(): Class<ProjectTemplate> {
        return ProjectTemplate::class.java
    }
}