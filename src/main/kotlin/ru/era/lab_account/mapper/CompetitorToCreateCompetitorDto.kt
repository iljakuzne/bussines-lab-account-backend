package ru.era.lab_account.mapper

import org.modelmapper.AbstractConverter
import org.springframework.stereotype.Service
import ru.era.lab_account.domain.Competitor
import ru.era.lab_account.dto.competitor.CreateCompetitorDto

@Service
class CompetitorToCreateCompetitorDto : MapperService<Competitor,CreateCompetitorDto> {

    override fun getConverter(): AbstractConverter<Competitor, CreateCompetitorDto> {
        return object : AbstractConverter<Competitor, CreateCompetitorDto>(){

            override fun convert(source: Competitor): CreateCompetitorDto {
                return CreateCompetitorDto(id = source.id,
                    competitorName = source.competitorName,
                    advantages = source.advantages,
                    problemSolved = source.problemSolved)
            }
        }
    }

    override fun sourceClass(): Class<Competitor> {
        return Competitor::class.java
    }

    override fun destinationClass(): Class<CreateCompetitorDto> {
        return CreateCompetitorDto::class.java
    }
}