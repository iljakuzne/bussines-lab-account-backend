package ru.era.lab_account.mapper

import org.modelmapper.AbstractConverter
import org.springframework.stereotype.Service
import ru.era.lab_account.domain.BussinesUser
import ru.era.lab_account.models.user.UserModel
import java.time.Instant

@Service
class BussinesUserToUserModel : MapperService<BussinesUser,UserModel> {


    override fun getConverter(): AbstractConverter<BussinesUser, UserModel> {
        return object : AbstractConverter<BussinesUser, UserModel>() {
            override fun convert(source: BussinesUser): UserModel {
                return UserModel(userId = source.id!!,
                    userRole = source.role,
                    login = source.login,
                    created = Instant.now(),
                    timeLeaf = 900)
            }
        }
    }

    override fun sourceClass(): Class<BussinesUser> {
        return BussinesUser::class.java
    }

    override fun destinationClass(): Class<UserModel> {
        return UserModel::class.java
    }
}