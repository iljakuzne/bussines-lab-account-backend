package ru.era.lab_account.mapper

import org.modelmapper.AbstractConverter
import org.springframework.stereotype.Service
import ru.era.lab_account.dto.project.ProjectCardDto
import ru.era.lab_account.models.project.CardProjectModel

@Service
class CardProjectModelToProjectCardDto : MapperService<CardProjectModel, ProjectCardDto> {

    override fun getConverter(): AbstractConverter<CardProjectModel, ProjectCardDto> {
        return object : AbstractConverter<CardProjectModel, ProjectCardDto>(){

            override fun convert(source: CardProjectModel): ProjectCardDto {
                return ProjectCardDto(id = source.templateProject.id!!,
                    userName = source.bussinessUser.login,
                    projectName = source.templateProject.projectName,
                    projectTarget = source.templateProject.projectTarget,
                    scopeName = source.scopeName,
                    createDate = source.templateProject.createDate,
                    userId = source.bussinessUser.id!!,
                    userEmail = source.bussinessUser.email,
                    userPhone = source.bussinessUser.phone?:"",
                    status = source.templateProject.status.number,
                    advantages = source.templateProject.advantages?:"",
                    problemSolved = source.templateProject.problemSolved?:"")
            }

        }
    }

    override fun sourceClass(): Class<CardProjectModel> {
        return CardProjectModel::class.java
    }

    override fun destinationClass(): Class<ProjectCardDto> {
        return ProjectCardDto::class.java
    }
}