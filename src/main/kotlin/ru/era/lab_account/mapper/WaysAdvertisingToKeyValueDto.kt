package ru.era.lab_account.mapper

import org.modelmapper.AbstractConverter
import org.springframework.stereotype.Service
import ru.era.lab_account.domain.WaysAdvertising
import ru.era.lab_account.dto.helper.KeyValueDto

@Service
class WaysAdvertisingToKeyValueDto : MapperService<WaysAdvertising, KeyValueDto>{
    override fun getConverter(): AbstractConverter<WaysAdvertising, KeyValueDto> {
        return object : AbstractConverter<WaysAdvertising, KeyValueDto>() {

            override fun convert(source: WaysAdvertising): KeyValueDto {
                return KeyValueDto(id = source.id!!,
                    value = source.name)
            }
        }
    }

    override fun sourceClass(): Class<WaysAdvertising> {
        return WaysAdvertising::class.java
    }

    override fun destinationClass(): Class<KeyValueDto> {
        return KeyValueDto::class.java
    }
}