package ru.era.lab_account.mapper

import org.modelmapper.AbstractConverter
import org.springframework.stereotype.Service
import ru.era.lab_account.domain.view.MarkResultView
import ru.era.lab_account.dto.mark.MarkDto

@Service
class MarkResultViewToMarkDto : MapperService<MarkResultView, MarkDto> {

    override fun getConverter(): AbstractConverter<MarkResultView, MarkDto> {
        return object : AbstractConverter<MarkResultView, MarkDto>() {

            override fun convert(source: MarkResultView): MarkDto {
                return MarkDto(mark = source.summa,
                    criteriaId = source.id,
                    criteriaName = source.criteriaName)
            }

        }
    }

    override fun sourceClass(): Class<MarkResultView> {
        return MarkResultView::class.java
    }

    override fun destinationClass(): Class<MarkDto> {
        return MarkDto::class.java
    }
}