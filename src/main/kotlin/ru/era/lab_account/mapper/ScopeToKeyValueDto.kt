package ru.era.lab_account.mapper

import org.modelmapper.AbstractConverter
import org.springframework.stereotype.Service
import ru.era.lab_account.domain.Scope
import ru.era.lab_account.dto.helper.KeyValueDto

@Service
class ScopeToKeyValueDto : MapperService<Scope, KeyValueDto> {

    override fun getConverter(): AbstractConverter<Scope, KeyValueDto> {
        return object : AbstractConverter<Scope, KeyValueDto> (){

            override fun convert(source: Scope): KeyValueDto {
                return KeyValueDto(id = source.id!!,
                    value = source.name)
            }

        }
    }

    override fun sourceClass(): Class<Scope> {
        return Scope::class.java
    }

    override fun destinationClass(): Class<KeyValueDto> {
        return KeyValueDto::class.java
    }
}