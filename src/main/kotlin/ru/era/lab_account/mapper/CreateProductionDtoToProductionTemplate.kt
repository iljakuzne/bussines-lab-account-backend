package ru.era.lab_account.mapper

import org.modelmapper.AbstractConverter
import org.springframework.stereotype.Service
import ru.era.lab_account.domain.ProductionTemplate
import ru.era.lab_account.dto.production.CreateProductionDto

@Service
class CreateProductionDtoToProductionTemplate : MapperService<CreateProductionDto, ProductionTemplate> {

    override fun getConverter(): AbstractConverter<CreateProductionDto, ProductionTemplate> {
        return object :AbstractConverter <CreateProductionDto, ProductionTemplate> () {

            override fun convert(source: CreateProductionDto): ProductionTemplate {
                return ProductionTemplate (projectId = source.projectId,
                    costPrise = source.costPrise,
                    timeProduction = source.timeProduction)
            }

        }
    }

    override fun sourceClass(): Class<CreateProductionDto> {
        return CreateProductionDto::class.java
    }

    override fun destinationClass(): Class<ProductionTemplate> {
        return ProductionTemplate::class.java
    }
}