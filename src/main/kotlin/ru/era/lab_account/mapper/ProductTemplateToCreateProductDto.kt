package ru.era.lab_account.mapper

import org.modelmapper.AbstractConverter
import org.springframework.stereotype.Service
import ru.era.lab_account.domain.ProductTemplate
import ru.era.lab_account.dto.product.CreateProductDto

@Service
class ProductTemplateToCreateProductDto : MapperService<ProductTemplate,CreateProductDto> {

    override fun getConverter(): AbstractConverter<ProductTemplate, CreateProductDto> {
        return object : AbstractConverter<ProductTemplate, CreateProductDto>(){

            override fun convert(source: ProductTemplate): CreateProductDto {
                return CreateProductDto(id = source.id,
                    projectId = source.projectId,
                    productName = source.productName,
                    productFunctional = source.productFunctional,
                    productCharacteristics = source.productCharacteristics,
                    usingProductId = source.usingProductId,
                    productAdvantages = source.productAdvantages,
                    additionalInformation = source.additionalInformation)
            }

        }
    }

    override fun sourceClass(): Class<ProductTemplate> {
        return ProductTemplate::class.java
    }

    override fun destinationClass(): Class<CreateProductDto> {
        return CreateProductDto::class.java
    }
}