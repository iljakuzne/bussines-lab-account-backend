package ru.era.lab_account.mapper

import org.modelmapper.AbstractConverter
import org.springframework.stereotype.Service
import ru.era.lab_account.dto.product.ProductCardDto
import ru.era.lab_account.models.product.ProductWithUsingNameModel

@Service
class ProductWithUsingNameModelToProductCardDto : MapperService<ProductWithUsingNameModel,ProductCardDto> {

    override fun getConverter(): AbstractConverter<ProductWithUsingNameModel, ProductCardDto> {
        return object : AbstractConverter<ProductWithUsingNameModel, ProductCardDto>(){

            override fun convert(source: ProductWithUsingNameModel): ProductCardDto {
                return ProductCardDto(id = source.productTemplate.id!!,
                    productName = source.productTemplate.productName,
                    projectId = source.productTemplate.projectId,
                    productFunction = source.productTemplate.productFunctional,
                    usingProductName = source.usingProductName)
            }

        }
    }

    override fun sourceClass(): Class<ProductWithUsingNameModel> {
        return ProductWithUsingNameModel::class.java
    }

    override fun destinationClass(): Class<ProductCardDto> {
        return ProductCardDto::class.java
    }
}