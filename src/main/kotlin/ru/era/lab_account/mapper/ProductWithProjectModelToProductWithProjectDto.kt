package ru.era.lab_account.mapper

import org.modelmapper.AbstractConverter
import org.springframework.stereotype.Service
import ru.era.lab_account.dto.product_with_project.ProductWithProjectDto
import ru.era.lab_account.models.product_with_project.ProductWithProjectModel


@Service
class ProductWithProjectModelToProductWithProjectDto : MapperService<ProductWithProjectModel, ProductWithProjectDto> {

    override fun getConverter(): AbstractConverter<ProductWithProjectModel, ProductWithProjectDto> {
        return object  : AbstractConverter<ProductWithProjectModel, ProductWithProjectDto>(){

            override fun convert(source: ProductWithProjectModel): ProductWithProjectDto {
                return ProductWithProjectDto(id = source.projectTemplate.id!!,
                    projectName=source.projectTemplate.projectName,
                    projectTarget=source.projectTemplate.projectTarget,
                    problemSolved=source.projectTemplate.problemSolved?:"",
                    advantages=source.projectTemplate.advantages?:"",
                    statusId=source.projectTemplate.status.number,
                    createDate=source.projectTemplate.createDate,
                    productId=source.productTemplate.id!!,
                    productName=source.productTemplate.productName,
                    productFunctional=source.productTemplate.productFunctional,
                    productCharacteristic=source.productTemplate.productCharacteristics!!,
                    additionalInformation=source.productTemplate.additionalInformation!!,
                    productAdvantages=source.productTemplate.productAdvantages,
                    userId=source.bussinesUser.id!!,
                    username=source.bussinesUser.login,
                    phone=source.bussinesUser.phone?:"",
                    email=source.bussinesUser.email,
                    role=source.bussinesUser.role)
            }

        }
    }

    override fun sourceClass(): Class<ProductWithProjectModel> {
        return ProductWithProjectModel::class.java
    }

    override fun destinationClass(): Class<ProductWithProjectDto> {
        return ProductWithProjectDto::class.java
    }
}