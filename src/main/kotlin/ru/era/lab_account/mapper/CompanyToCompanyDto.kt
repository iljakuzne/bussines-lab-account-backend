package ru.era.lab_account.mapper

import org.modelmapper.AbstractConverter
import org.springframework.stereotype.Service
import ru.era.lab_account.domain.Company
import ru.era.lab_account.dto.helper.KeyValueDto

@Service
class CompanyToCompanyDto : MapperService<Company, KeyValueDto> {

    override fun getConverter(): AbstractConverter<Company, KeyValueDto> {
        return object : AbstractConverter<Company, KeyValueDto>() {
            override fun convert(source: Company): KeyValueDto {
                return KeyValueDto(id = source.id,
                    value = source.companyName)
            }

        }
    }

    override fun sourceClass(): Class<Company> {
        return Company::class.java
    }

    override fun destinationClass(): Class<KeyValueDto> {
        return KeyValueDto::class.java
    }
}