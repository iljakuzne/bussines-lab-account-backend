package ru.era.lab_account.mapper

import org.modelmapper.AbstractConverter

interface MapperService <Source,Destination> {

    fun getConverter () : AbstractConverter<Source, Destination>

    fun sourceClass () : Class<Source>

    fun destinationClass () : Class<Destination>
}