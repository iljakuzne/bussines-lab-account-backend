package ru.era.lab_account.mapper

import org.modelmapper.AbstractConverter
import org.springframework.stereotype.Service
import ru.era.lab_account.domain.StageRealization
import ru.era.lab_account.dto.helper.KeyValueDto

@Service
class StageRealizationToKeyValueDto : MapperService<StageRealization, KeyValueDto> {

    override fun getConverter(): AbstractConverter<StageRealization, KeyValueDto> {
        return object : AbstractConverter<StageRealization, KeyValueDto>(){

            override fun convert(source: StageRealization): KeyValueDto {
                return KeyValueDto(id = source.id!!,
                value = source.stageRealization)
            }

        }
    }

    override fun sourceClass(): Class<StageRealization> {
        return StageRealization::class.java
    }

    override fun destinationClass(): Class<KeyValueDto> {
        return KeyValueDto::class.java
    }
}