package ru.era.lab_account.mapper

import org.modelmapper.AbstractConverter
import org.springframework.stereotype.Service
import ru.era.lab_account.dto.marketing.MarketingCardDto
import ru.era.lab_account.models.marketing.CardMarketingModel

@Service
class CardMarketingModelToMarketingCardDto : MapperService<CardMarketingModel, MarketingCardDto> {
    override fun getConverter(): AbstractConverter<CardMarketingModel, MarketingCardDto> {
        return object : AbstractConverter<CardMarketingModel, MarketingCardDto> () {

            override fun convert(source: CardMarketingModel): MarketingCardDto {
                return MarketingCardDto(id = source.templateMarketing.id!!,
                    productName = source.productName,
                    marketingProductCost = source.templateMarketing.marketingProductCost,
                    volumeSales = source.templateMarketing.volumeSales,
                    marketingCost = source.templateMarketing.marketingCost)
            }

        }
    }

    override fun sourceClass(): Class<CardMarketingModel> {
        return CardMarketingModel::class.java
    }

    override fun destinationClass(): Class<MarketingCardDto> {
        return MarketingCardDto::class.java
    }
}