package ru.era.lab_account.mapper

import org.modelmapper.AbstractConverter
import org.springframework.stereotype.Service
import ru.era.lab_account.dto.market.CreateMarketDto
import ru.era.lab_account.models.market.MarketWithCompetitorsDto

@Service
class MarketToCreateMarketDto : MapperService<MarketWithCompetitorsDto,CreateMarketDto> {

    override fun getConverter(): AbstractConverter<MarketWithCompetitorsDto, CreateMarketDto> {
        return object : AbstractConverter<MarketWithCompetitorsDto, CreateMarketDto>(){

            override fun convert(source: MarketWithCompetitorsDto): CreateMarketDto {
                return CreateMarketDto(id=source.market.id ,
                    projectId = source.market.projectId,
                    barrierEntry = source.market.barrierEntry,
                    outputAnaliz = source.market.outputAnaliz,
                    regionId = source.market.regionId,
                    competitors = source.competitors)
            }

        }
    }

    override fun sourceClass(): Class<MarketWithCompetitorsDto> {
        return MarketWithCompetitorsDto::class.java
    }

    override fun destinationClass(): Class<CreateMarketDto> {
        return CreateMarketDto::class.java
    }
}