package ru.era.lab_account.mapper

import org.modelmapper.AbstractConverter
import org.springframework.stereotype.Service
import ru.era.lab_account.domain.MarketingTemplate
import ru.era.lab_account.dto.marketing.CreateMarketingDto
import ru.era.lab_account.models.marketing.CreateMarketing

@Service
class CreateMarketingDtoToMarketingTemplate : MapperService<CreateMarketingDto, MarketingTemplate> {

    override fun getConverter(): AbstractConverter<CreateMarketingDto, MarketingTemplate> {
        return object : AbstractConverter<CreateMarketingDto, MarketingTemplate> () {

            override fun convert(source: CreateMarketingDto): MarketingTemplate {
                return MarketingTemplate(projectId = source.projectId,
                    marketingProductCost = source.marketingProductCost,
                    volumeSales = source.volumeSales,
                    marketingCost = source.marketingCost
                )
            }
        }
    }

    override fun sourceClass(): Class<CreateMarketingDto> {
        return CreateMarketingDto::class.java
    }

    override fun destinationClass(): Class<MarketingTemplate> {
        return MarketingTemplate::class.java
    }
}