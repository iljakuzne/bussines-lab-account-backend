package ru.era.lab_account.mapper

import org.modelmapper.AbstractConverter
import org.springframework.stereotype.Service
import ru.era.lab_account.domain.ProjectTemplate
import ru.era.lab_account.dto.project.CreateProjectDto

@Service
class ProjectTemplateToCreateProjectDto : MapperService<ProjectTemplate, CreateProjectDto> {

    override fun getConverter(): AbstractConverter<ProjectTemplate, CreateProjectDto> {
        return object : AbstractConverter<ProjectTemplate, CreateProjectDto>(){

            override fun convert(source: ProjectTemplate): CreateProjectDto {
                return CreateProjectDto(id = source.id,
                    projectName = source.projectName,
                    projectTarget = source.projectTarget,
                    solvedProblem = source.problemSolved,
                    advantages = source.advantages,
                    scopeId = source.scopeId,
                    stageId = source.stageRealizationId,
                    readyTime = source.implementData,
                )
            }

        }
    }

    override fun sourceClass(): Class<ProjectTemplate> {
        return ProjectTemplate::class.java
    }

    override fun destinationClass(): Class<CreateProjectDto> {
        return CreateProjectDto::class.java
    }
}