package ru.era.lab_account.mapper

import org.modelmapper.AbstractConverter
import org.springframework.stereotype.Service
import ru.era.lab_account.dto.competitors.CreateCompetitorsDto
import ru.era.lab_account.models.competitors.CreateCompetitors

@Service
class CreateCompetitorsDtoToCompetitorsTemplate : MapperService<CreateCompetitors, CreateCompetitorsDto> {

    override fun getConverter(): AbstractConverter<CreateCompetitors, CreateCompetitorsDto> {
        return object : AbstractConverter <CreateCompetitors, CreateCompetitorsDto>() {

            override fun convert(source: CreateCompetitors): CreateCompetitorsDto {
                return CreateCompetitorsDto(competitorsName = source.competitorsTemplate.competitorsName,
                    competitorsAdvantages = source.competitorsTemplate.competitorsAdvantages?:"",
                    problemSolved = source.competitorsTemplate.problemSolved?:"")
            }

        }
    }

    override fun sourceClass(): Class<CreateCompetitors> {
        return CreateCompetitors::class.java
    }

    override fun destinationClass(): Class<CreateCompetitorsDto> {
        return CreateCompetitorsDto::class.java
    }
}