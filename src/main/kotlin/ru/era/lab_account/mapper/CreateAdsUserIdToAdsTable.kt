package ru.era.lab_account.mapper

import org.modelmapper.AbstractConverter
import org.springframework.stereotype.Service
import ru.era.lab_account.domain.AdsTable
import ru.era.lab_account.models.ads.AdsDtoWithUserIdModel
import java.time.Instant

@Service
class CreateAdsUserIdToAdsTable : MapperService<AdsDtoWithUserIdModel,AdsTable> {

    override fun getConverter(): AbstractConverter<AdsDtoWithUserIdModel, AdsTable> {
        return object : AbstractConverter<AdsDtoWithUserIdModel, AdsTable>() {

            override fun convert(source: AdsDtoWithUserIdModel): AdsTable {
                return AdsTable(name=source.createAdsDto.name,
                    content = source.createAdsDto.content,
                    created = Instant.now(),
                    userId = source.userId)
            }
        }
    }

    override fun sourceClass(): Class<AdsDtoWithUserIdModel> {
        return AdsDtoWithUserIdModel::class.java
    }

    override fun destinationClass(): Class<AdsTable> {
        return AdsTable::class.java
    }
}