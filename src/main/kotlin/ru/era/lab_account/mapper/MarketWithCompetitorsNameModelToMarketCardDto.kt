package ru.era.lab_account.mapper

import org.modelmapper.AbstractConverter
import org.springframework.stereotype.Service
import ru.era.lab_account.dto.market.MarketCardDto
import ru.era.lab_account.models.market.MarketWithCompetitorsNameModel


@Service
class MarketWithCompetitorsNameModelToMarketCardDto :MapperService<MarketWithCompetitorsNameModel, MarketCardDto> {
    override fun getConverter(): AbstractConverter<MarketWithCompetitorsNameModel, MarketCardDto> {
        return object : AbstractConverter<MarketWithCompetitorsNameModel, MarketCardDto> () {

            override fun convert(source: MarketWithCompetitorsNameModel): MarketCardDto {
                return MarketCardDto(id = source.marketTemplate.id!!,
                    competitorsName = source.competitorsName)
            }

        }
    }

    override fun sourceClass(): Class<MarketWithCompetitorsNameModel> {
        return MarketWithCompetitorsNameModel::class.java
    }

    override fun destinationClass(): Class<MarketCardDto> {
        return MarketCardDto::class.java
    }
}