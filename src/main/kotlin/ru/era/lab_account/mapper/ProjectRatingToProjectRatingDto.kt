package ru.era.lab_account.mapper

import org.modelmapper.AbstractConverter
import org.springframework.stereotype.Service
import ru.era.lab_account.domain.view.ProjectCardRating
import ru.era.lab_account.dto.project.ProjectRatingDto

@Service
class ProjectRatingToProjectRatingDto : MapperService<ProjectCardRating,ProjectRatingDto> {

    override fun getConverter(): AbstractConverter<ProjectCardRating, ProjectRatingDto> {
        return object :AbstractConverter<ProjectCardRating, ProjectRatingDto> (){

            override fun convert(source: ProjectCardRating): ProjectRatingDto {
                return ProjectRatingDto(id = source.id,
                    userId = source.userId,
                    userName = source.userName,
                    userEmail = source.userEmail,
                    userPhone = source.userPhone,
                    projectName = source.projectName,
                    projectTarget = source.projectTarget,
                    advantages = source.advantages,
                    problemSolved = source.problemSolved,
                    status = source.status.number,
                    createDate = source.createDate,
                    rating = source.rating)
            }

        }
    }

    override fun sourceClass(): Class<ProjectCardRating> {
        return ProjectCardRating::class.java
    }

    override fun destinationClass(): Class<ProjectRatingDto> {
        return ProjectRatingDto::class.java
    }
}