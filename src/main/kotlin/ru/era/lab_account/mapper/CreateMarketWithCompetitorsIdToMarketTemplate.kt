package ru.era.lab_account.mapper

import org.modelmapper.AbstractConverter
import org.springframework.stereotype.Service
import ru.era.lab_account.domain.MarketTemplate
import ru.era.lab_account.models.market.CreateMarketWithCompetitorsId

@Service
class CreateMarketWithCompetitorsIdToMarketTemplate :MapperService<CreateMarketWithCompetitorsId, MarketTemplate> {
    override fun getConverter(): AbstractConverter<CreateMarketWithCompetitorsId, MarketTemplate> {
        return object : AbstractConverter<CreateMarketWithCompetitorsId, MarketTemplate> () {

            override fun convert(source: CreateMarketWithCompetitorsId): MarketTemplate {
                return MarketTemplate(projectId = source.marketTemplate.projectId,
                    barrierEntry = source.marketTemplate.barrierEntry,
                    outputAnaliz = source.marketTemplate.outputAnaliz,
                    regionId = source.marketTemplate.regionId)
            }

        }
    }

    override fun sourceClass(): Class<CreateMarketWithCompetitorsId> {
        return CreateMarketWithCompetitorsId::class.java
    }

    override fun destinationClass(): Class<MarketTemplate> {
        return MarketTemplate::class.java
    }
}