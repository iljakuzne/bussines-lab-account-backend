package ru.era.lab_account.mapper

import org.modelmapper.AbstractConverter
import org.springframework.stereotype.Service
import ru.era.lab_account.domain.MarkCriteria
import ru.era.lab_account.dto.helper.KeyValueDto

@Service
class MarkCriteriaToKeyValueDto : MapperService<MarkCriteria,KeyValueDto> {

    override fun getConverter(): AbstractConverter<MarkCriteria, KeyValueDto> {
        return object: AbstractConverter<MarkCriteria, KeyValueDto>(){

            override fun convert(source: MarkCriteria): KeyValueDto {
                return KeyValueDto(id = source.id,
                    value = source.criteriaName)
            }

        }
    }

    override fun sourceClass(): Class<MarkCriteria> {
        return MarkCriteria::class.java
    }

    override fun destinationClass(): Class<KeyValueDto> {
        return KeyValueDto::class.java
    }
}