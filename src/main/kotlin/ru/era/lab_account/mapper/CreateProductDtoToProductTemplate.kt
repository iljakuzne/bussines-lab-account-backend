package ru.era.lab_account.mapper

import org.modelmapper.AbstractConverter
import org.springframework.stereotype.Service
import ru.era.lab_account.domain.ProductTemplate
import ru.era.lab_account.dto.product.CreateProductDto

@Service
class CreateProductDtoToProductTemplate : MapperService<CreateProductDto, ProductTemplate>{

    override fun getConverter(): AbstractConverter<CreateProductDto, ProductTemplate> {
        return object : AbstractConverter<CreateProductDto, ProductTemplate>(){

            override fun convert(source: CreateProductDto): ProductTemplate {
                return ProductTemplate(projectId = source.projectId,
                    productName = source.productName,
                    productFunctional = source.productFunctional,
                    productCharacteristics = source.productCharacteristics,
                    usingProductId = source.usingProductId,
                    productAdvantages = source.productAdvantages,
                    additionalInformation = source.additionalInformation)
            }

        }
    }

    override fun sourceClass(): Class<CreateProductDto> {
        return CreateProductDto::class.java
    }

    override fun destinationClass(): Class<ProductTemplate> {
        return ProductTemplate::class.java
    }
}