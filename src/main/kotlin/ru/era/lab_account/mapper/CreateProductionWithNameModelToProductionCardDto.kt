package ru.era.lab_account.mapper

import org.modelmapper.AbstractConverter
import ru.era.lab_account.dto.production.ProductionCardDto
import ru.era.lab_account.models.production.CreateProductionWithNameModel

class CreateProductionWithNameModelToProductionCardDto : MapperService <CreateProductionWithNameModel, ProductionCardDto> {

    override fun getConverter(): AbstractConverter<CreateProductionWithNameModel, ProductionCardDto> {
        return object : AbstractConverter <CreateProductionWithNameModel, ProductionCardDto> () {

            override fun convert(source: CreateProductionWithNameModel): ProductionCardDto {
                return ProductionCardDto (id = source.production.id!!,
                    projectName = source.projectName,
                    costPrise = source.production.costPrise,
                    timeProduction = source.production.timeProduction)
            }

        }
    }

    override fun sourceClass(): Class<CreateProductionWithNameModel> {
        return CreateProductionWithNameModel::class.java
    }

    override fun destinationClass(): Class<ProductionCardDto> {
        return ProductionCardDto::class.java
    }
}