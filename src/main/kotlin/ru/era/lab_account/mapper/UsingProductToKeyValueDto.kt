package ru.era.lab_account.mapper

import org.modelmapper.AbstractConverter
import org.springframework.stereotype.Service
import ru.era.lab_account.domain.UsingProduct
import ru.era.lab_account.dto.helper.KeyValueDto

@Service
class UsingProductToKeyValueDto :MapperService<UsingProduct, KeyValueDto>{
    override fun getConverter(): AbstractConverter<UsingProduct, KeyValueDto> {
        return object : AbstractConverter<UsingProduct, KeyValueDto>(){

            override fun convert(source: UsingProduct): KeyValueDto {
                return KeyValueDto(id = source.id!!,
                value = source.name)
            }

        }
    }

    override fun sourceClass(): Class<UsingProduct> {
        return UsingProduct::class.java
    }

    override fun destinationClass(): Class<KeyValueDto> {
        return KeyValueDto::class.java
    }
}