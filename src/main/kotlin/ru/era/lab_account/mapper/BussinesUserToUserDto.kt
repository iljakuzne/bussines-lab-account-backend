package ru.era.lab_account.mapper

import org.modelmapper.AbstractConverter
import org.springframework.stereotype.Service
import ru.era.lab_account.domain.BussinesUser
import ru.era.lab_account.dto.user.UserDto

@Service
class BussinesUserToUserDto : MapperService<BussinesUser, UserDto> {

    override fun getConverter(): AbstractConverter<BussinesUser, UserDto> {
        return object : AbstractConverter<BussinesUser, UserDto> (){

            override fun convert(source: BussinesUser): UserDto {
                return UserDto(id = source.id?:0,
                login = source.login,
                phone = source.phone?:"",
                email = source.email,
                role = source.role)
            }
        }
    }

    override fun sourceClass(): Class<BussinesUser> {
        return BussinesUser::class.java
    }

    override fun destinationClass(): Class<UserDto> {
        return UserDto::class.java
    }
}