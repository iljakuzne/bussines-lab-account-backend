package ru.era.lab_account.mapper

import org.modelmapper.AbstractConverter
import org.springframework.stereotype.Service
import ru.era.lab_account.domain.Competitor
import ru.era.lab_account.models.competitior.CompetitorWithMarketId

@Service
class CreateCompetitorDtoToCompetitor : MapperService<CompetitorWithMarketId, Competitor> {

    override fun getConverter(): AbstractConverter<CompetitorWithMarketId, Competitor> {
        return object : AbstractConverter<CompetitorWithMarketId, Competitor>(){

            override fun convert(source: CompetitorWithMarketId): Competitor {
                return Competitor(marketId = source.marketId,
                    competitorName = source.competitor.competitorName,
                    advantages = source.competitor.advantages,
                    problemSolved = source.competitor.problemSolved)
            }

        }
    }

    override fun sourceClass(): Class<CompetitorWithMarketId> {
        return CompetitorWithMarketId::class.java
    }

    override fun destinationClass(): Class<Competitor> {
        return Competitor::class.java
    }
}