package ru.era.lab_account.mapper

import org.modelmapper.AbstractConverter
import org.springframework.stereotype.Service
import ru.era.lab_account.dto.ads.AdsTableDto
import ru.era.lab_account.models.ads.AdsTableWithBussinessUser

@Service
class AdsTableWithBussinessUserToAdsDto : MapperService<AdsTableWithBussinessUser,AdsTableDto> {

    override fun getConverter(): AbstractConverter<AdsTableWithBussinessUser, AdsTableDto> {
        return object : AbstractConverter<AdsTableWithBussinessUser, AdsTableDto>(){

            override fun convert(source: AdsTableWithBussinessUser): AdsTableDto {
                return AdsTableDto(id = source.adsTable.id!!,
                    created = source.adsTable.created,
                    name = source.adsTable.name,
                    content = source.adsTable.content,
                    userId = source.user.id!!,
                    userName = source.user.login,
                    userEmail = source.user.email,
                    userPhone = source.user.phone?:"")
            }

        }
    }

    override fun sourceClass(): Class<AdsTableWithBussinessUser> {
        return AdsTableWithBussinessUser::class.java
    }

    override fun destinationClass(): Class<AdsTableDto> {
        return AdsTableDto::class.java
    }
}