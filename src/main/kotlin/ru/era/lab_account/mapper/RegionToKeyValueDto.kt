package ru.era.lab_account.mapper

import org.modelmapper.AbstractConverter
import org.springframework.stereotype.Service
import ru.era.lab_account.domain.Region
import ru.era.lab_account.dto.helper.KeyValueDto

@Service
class RegionToKeyValueDto : MapperService<Region, KeyValueDto> {

    override fun getConverter(): AbstractConverter<Region, KeyValueDto> {
        return object : AbstractConverter <Region, KeyValueDto> () {

            override fun convert(source: Region): KeyValueDto {
                return KeyValueDto(id = source.id!!,
                value = source.name)
            }

        }
    }

    override fun sourceClass(): Class<Region> {
        return Region::class.java
    }

    override fun destinationClass(): Class<KeyValueDto> {
        return KeyValueDto::class.java
    }
}