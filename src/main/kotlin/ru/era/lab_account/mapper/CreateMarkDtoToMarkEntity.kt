package ru.era.lab_account.mapper

import org.modelmapper.AbstractConverter
import org.springframework.stereotype.Service
import ru.era.lab_account.domain.Mark
import ru.era.lab_account.models.mark.MarkWithUserId

@Service
class CreateMarkDtoToMarkEntity : MapperService<MarkWithUserId,Mark> {

    override fun getConverter(): AbstractConverter<MarkWithUserId, Mark> {
        return object: AbstractConverter<MarkWithUserId, Mark>() {

            override fun convert(source: MarkWithUserId): Mark {
                return Mark(
                    userId = source.userId,
                    markCriteria = source.createMarkDto.criteriaId,
                    mark = source.createMarkDto.mark,
                    projectId = source.createMarkDto.projectId)
            }

        }
    }

    override fun sourceClass(): Class<MarkWithUserId> {
        return MarkWithUserId::class.java
    }

    override fun destinationClass(): Class<Mark> {
        return Mark::class.java
    }
}