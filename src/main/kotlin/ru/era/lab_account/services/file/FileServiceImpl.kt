package ru.era.lab_account.services.file

import org.springframework.http.HttpHeaders.CONTENT_DISPOSITION
import org.springframework.http.HttpHeaders.CONTENT_TYPE
import org.springframework.http.MediaType
import org.springframework.http.ZeroCopyHttpOutputMessage
import org.springframework.http.codec.multipart.FilePart
import org.springframework.http.server.reactive.ServerHttpResponse
import org.springframework.stereotype.Service
import reactor.core.publisher.Mono
import ru.era.lab_account.domain.FileProject
import java.io.File
import java.nio.file.Path


@Service
class FileServiceImpl : FileService {

    override fun uploadFile(filePart: Mono<FilePart>, path: Path): Mono<Boolean> {
        return filePart.map(::FilePartDecorator)
            .flatMap {decorator->
                decorator.save(path)
            }
    }

    override fun downloadFile(fileProject: FileProject, httpResponse: ServerHttpResponse): Mono<Void> {
        val zeroCopyResponse = httpResponse as ZeroCopyHttpOutputMessage
        httpResponse.getHeaders()[CONTENT_DISPOSITION] = fileProject.contentType
        httpResponse.getHeaders()[CONTENT_TYPE] = MediaType.APPLICATION_OCTET_STREAM_VALUE
        val file = File(fileProject.path);
        return zeroCopyResponse.writeWith(file, 0, file.length())
    }


}