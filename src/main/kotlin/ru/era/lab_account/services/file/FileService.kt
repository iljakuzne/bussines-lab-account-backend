package ru.era.lab_account.services.file

import org.springframework.http.codec.multipart.FilePart
import org.springframework.http.server.reactive.ServerHttpResponse
import reactor.core.publisher.Mono
import ru.era.lab_account.domain.FileProject
import java.nio.file.Path

interface FileService {

    fun uploadFile (filePart: Mono<FilePart>, path: Path) : Mono<Boolean>

    fun downloadFile (fileProject: FileProject, httpResponse: ServerHttpResponse) : Mono<Void>
}