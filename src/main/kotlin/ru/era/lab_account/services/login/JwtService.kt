package ru.era.lab_account.services.login

import io.jsonwebtoken.Claims
import io.jsonwebtoken.Jws
import ru.era.lab_account.models.user.UserModel

/**
 * Сервис для создания и валидации токена пользователя
 */
interface JwtService {

    /**
     * Создание Jwt токена
     */
    fun createJwt (userModel: UserModel): String

    /**
     * Валидация Jwt токена
     */
    fun validateJwt (jwt: String): Jws<Claims>
}