package ru.era.lab_account.services.login

import org.springframework.stereotype.Service
import java.io.BufferedInputStream
import java.nio.charset.StandardCharsets
import java.nio.file.Files
import java.nio.file.Path
import java.security.MessageDigest

@Service
class HashServiceImpl(private val digest: MessageDigest = MessageDigest.getInstance("SHA3-256")) :
    HashService {

    override fun generateHash(password: String, salt: String): String {
        val stringForHashing = password + salt;
        val hashbytes = digest.digest(stringForHashing.toByteArray(StandardCharsets.UTF_8))
        return bytesToHex(hashbytes)
    }

    override fun getHashForFile(path: Path): String {
        var buffer = ByteArray(1024 * 100)

        BufferedInputStream(Files.newInputStream(path)).use {inputStream->
            var i = inputStream.read(buffer)
            while (i != -1) {
                digest.update(buffer, 0, i)
                i = inputStream.read(buffer)
            }
        }
        return bytesToHex(digest.digest())
    }

    private fun bytesToHex(hash: ByteArray): String {
        val hexString = StringBuilder(2 * hash.size)
        for (i in hash.indices) {
            val hex = Integer.toHexString(0xff and hash[i].toInt())
            if (hex.length == 1) {
                hexString.append('0')
            }
            hexString.append(hex)
        }
        return hexString.toString()
    }
}