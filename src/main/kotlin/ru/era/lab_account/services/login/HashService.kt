package ru.era.lab_account.services.login

import java.nio.file.Path

interface HashService {

    fun generateHash (password: String, salt: String) : String

    fun getHashForFile (path: Path) : String
}