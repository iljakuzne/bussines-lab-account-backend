package ru.era.lab_account.services.login

import org.modelmapper.ModelMapper
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import reactor.core.publisher.Mono
import reactor.core.publisher.SynchronousSink
import ru.era.lab_account.configuration.security.JwtUtils
import ru.era.lab_account.dao.BussinessUserRepository
import ru.era.lab_account.domain.BussinesUser
import ru.era.lab_account.dto.auth.AuthenticationDto
import ru.era.lab_account.dto.user.UserDto
import ru.era.lab_account.models.auth.AuthenticationModel
import ru.era.lab_account.models.user.UserModel

@Service
class AuthenticationServiceImpl(private val userRepository: BussinessUserRepository,
                                private val modelMapper: ModelMapper,
                                private val jwtUtils: JwtUtils,
                                private val hashService: HashService)
    : AuthenticationService {

    override fun authUser(authenticationDto: AuthenticationDto): Mono<AuthenticationModel> {
        return getUser(authenticationDto)
            .switchIfEmpty(Mono.error(IllegalArgumentException("Пользователь ${authenticationDto.userLogin} не существует")))
            .handle{user, synchronousSink: SynchronousSink<Pair<UserModel,BussinesUser>>->
                val hash =  hashService.generateHash(authenticationDto.userPassword,user.salt)
                if (hash == user.password){
                    val userModel = modelMapper.map(user,UserModel::class.java)
                    synchronousSink.next(Pair(userModel,user))
                } else {
                    synchronousSink.error(IllegalArgumentException("Пароль указан неверно"))
                }
            }.map { userModel->
                val jwtToken = jwtUtils.generateToken(userModel.first)
                AuthenticationModel(jwtToken,modelMapper.map(userModel.second, UserDto::class.java),userModel.first)
            }
    }

    @Transactional(readOnly = true)
    protected fun getUser (authenticationDto: AuthenticationDto) : Mono<BussinesUser> =
        userRepository.findByLogin(authenticationDto.userLogin)
}