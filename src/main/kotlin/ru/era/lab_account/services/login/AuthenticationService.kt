package ru.era.lab_account.services.login

import reactor.core.publisher.Mono
import ru.era.lab_account.dto.auth.AuthenticationDto
import ru.era.lab_account.models.auth.AuthenticationModel

interface AuthenticationService {

    /**
     * Возвращает JWT токен
     */
    fun authUser (authenticationDto: AuthenticationDto) : Mono<AuthenticationModel>
}