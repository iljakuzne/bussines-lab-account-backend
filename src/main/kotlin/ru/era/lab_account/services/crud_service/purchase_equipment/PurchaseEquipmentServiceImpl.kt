package ru.era.lab_account.services.crud_service.purchase_equipment

import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import ru.era.lab_account.dao.PurchaseEquipmentRepository
import ru.era.lab_account.domain.PurchaseEquipment
import ru.era.lab_account.dto.purchase_equipment.CreatePurchaseEquimentDto

@Service
class PurchaseEquipmentServiceImpl (private val purchaseEquipmentRepository: PurchaseEquipmentRepository) :  PurchaseEquipmentService{


    @Transactional
    override fun createPurchaseEquipment(productionId: Long, equipment: List<CreatePurchaseEquimentDto>)
            : Flux<PurchaseEquipment> {
        return Mono.fromCallable {
            equipment.map {dto->
                PurchaseEquipment(productionId=productionId,
                    name = dto.name,
                    cost = dto.cost)
            }
        }
            .flatMapMany {purchases->
                purchaseEquipmentRepository.saveAll(purchases)
            }
    }
}