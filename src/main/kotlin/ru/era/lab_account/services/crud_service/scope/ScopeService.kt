package ru.era.lab_account.services.crud_service.scope

import reactor.core.publisher.Flux
import ru.era.lab_account.dto.helper.KeyValueDto

interface ScopeService {

    fun findAll () : Flux<KeyValueDto>
}