package ru.era.lab_account.services.crud_service.file

import org.springframework.http.codec.multipart.FilePart
import org.springframework.http.server.reactive.ServerHttpResponse
import reactor.core.publisher.Mono
import ru.era.lab_account.dto.file.FileDto

interface FileEntityService {

    fun saveFile (file: Mono<FilePart>) : Mono<Long>

    fun downloadFile (fileId: Long,
                      response: ServerHttpResponse) : Mono<Void>
}