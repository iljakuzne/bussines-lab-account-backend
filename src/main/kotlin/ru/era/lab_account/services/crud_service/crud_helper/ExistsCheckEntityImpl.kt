package ru.era.lab_account.services.crud_service.crud_helper

import org.springframework.data.r2dbc.repository.R2dbcRepository
import org.springframework.stereotype.Service
import reactor.core.publisher.Mono
import reactor.core.publisher.SynchronousSink

@Service
class ExistsCheckEntityImpl : ExistsCheckEntity {

    override fun <Entity> validateExistsId(id: Long, repository: R2dbcRepository<Entity,Long>): Mono<Boolean> {
        return repository.existsById(id)
            .handle { exists, synchronousSink: SynchronousSink<Boolean> ->
                if (exists){
                    synchronousSink.next(exists)
                } else {
                    synchronousSink.error(IllegalArgumentException("Объект с идентификатором ${id} не существует"))
                }
            }
    }

    override fun <Entity> validateNotExistsId(id: Long, repository: R2dbcRepository<Entity,Long>): Mono<Boolean> {
        return repository.existsById(id)
            .handle { exists, synchronousSink: SynchronousSink<Boolean> ->
                if (exists){
                    synchronousSink.error(IllegalArgumentException("Объект с идентификатором ${id} уже создан"))
                } else {
                    synchronousSink.next(exists)
                }
            }
    }
}



