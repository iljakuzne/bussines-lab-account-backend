package ru.era.lab_account.services.crud_service.purchases_property

import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import ru.era.lab_account.dao.PurchasePropertyRepository
import ru.era.lab_account.domain.PurchaseProperty
import ru.era.lab_account.dto.purchase_property.CreatePurchasePropertyDto

@Service
class PurchasesPropertyServiceImpl(private val purchasesPropertyRepository: PurchasePropertyRepository) : PurchasesPropertyService{

    @Transactional
    override fun createPurchasesProperty(productionId: Long, properties: List<CreatePurchasePropertyDto>) : Flux<PurchaseProperty> {
        return Mono.fromCallable {
            properties.map{property->
                PurchaseProperty(productionId = productionId,
                    name = property.name,
                    cost = property.cost)
            }
        }
            .flatMapMany {purchases->
                purchasesPropertyRepository.saveAll(purchases)
            }
    }
}