package ru.era.lab_account.services.crud_service.ads_table

import org.modelmapper.ModelMapper
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import reactor.core.publisher.SynchronousSink
import ru.era.lab_account.dao.AdsTableRepository
import ru.era.lab_account.dao.BussinessUserRepository
import ru.era.lab_account.domain.AdsTable
import ru.era.lab_account.domain.BussinesUser
import ru.era.lab_account.dto.ads.AdsTableDto
import ru.era.lab_account.dto.ads.AdsTableDtoToUser
import ru.era.lab_account.dto.ads.CreateAdsDto
import ru.era.lab_account.models.ads.AdsDtoWithUserIdModel
import ru.era.lab_account.models.ads.AdsTableWithBussinessUser
import ru.era.lab_account.models.pagination.PaginationRequest
import ru.era.lab_account.services.crud_service.user_principal.UserTokenDecoder

@Service
class AdsTableServiceImpl (private val modelMapper: ModelMapper,
                           private val decoder: UserTokenDecoder,
                           private val bussinessUserRepository: BussinessUserRepository,
                           private val adsTableRepository: AdsTableRepository) : AdsTableService {

    @Transactional
    override fun createAds(createAdsDto: CreateAdsDto): Mono<Long> {
        return decoder.getUserIdByToken()
            .map {userId->
                val model = AdsDtoWithUserIdModel(userId,createAdsDto)
                modelMapper.map(model,AdsTable::class.java)
            }
            .flatMap {entity->
                adsTableRepository.save(entity)
            }
            .map(AdsTable::id!!)
    }

    @Transactional(readOnly = true)
    override fun getAllAds(paginationRequest: PaginationRequest): Flux<AdsTableDto> {
        return adsTableRepository.paginationFindAll(paginationRequest.limit, paginationRequest.pageCount)
            .collectList()
            .flatMapMany (::validateAdsTableDto)
    }

    @Transactional(readOnly = true)
    override fun getAdsByUser(paginationRequest: PaginationRequest): Flux<AdsTableDto> {
        return decoder.getUserIdByToken()
            .flatMapMany{userId->
                adsTableRepository.paginationFindByUser(paginationRequest.limit,
                    paginationRequest.pageCount,
                    userId)
                    .collectList()
                    .flatMapMany(::validateAdsTableDto)
            }
    }

    @Transactional(readOnly = true)
    protected fun validateAdsTableDto (adsTable: List<AdsTable>) : Flux<AdsTableDto> {
        return if (!adsTable.isEmpty()){
            val userIds = adsTable.map(AdsTable::userId).distinct()
            bussinessUserRepository.findUserByIds(userIds)
                .collectMap(BussinesUser::id!!)
                .flatMapMany {mapUser->
                    Flux.fromIterable(adsTable.map {ads->
                        val model = AdsTableWithBussinessUser(mapUser[ads.userId]!!,ads)
                        modelMapper.map(model,AdsTableDto::class.java)
                    })
                }
        } else {
            Flux.empty()
        }
    }

    @Transactional
    override fun deleteAds(adId: Long): Mono<Long> {
        return decoder.getUserIdByToken()
            .flatMap {userId->
                adsTableRepository.existsAdsByUserAndId(userId,adId)
            }
            .handle{exists, syncrhonousSink: SynchronousSink<Boolean>->
                if (exists){
                    syncrhonousSink.next(exists)
                } else {
                    syncrhonousSink.error(IllegalArgumentException("Объявление не существует или не принадлежит польхователю"))
                }
            }
            .flatMap {_->
                adsTableRepository.deleteById(adId)
                    .then(Mono.just(adId))
            }
    }


}