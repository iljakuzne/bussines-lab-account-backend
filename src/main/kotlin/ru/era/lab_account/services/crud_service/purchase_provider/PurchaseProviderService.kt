package ru.era.lab_account.services.crud_service.purchase_provider

import reactor.core.publisher.Flux
import ru.era.lab_account.domain.PurchaseProvider
import ru.era.lab_account.dto.purchase_provider.CreatePurchaseProviderDto

interface PurchaseProviderService {

    fun createPurchaseProviders (productionId: Long,
                                 providers: List<CreatePurchaseProviderDto>) : Flux<PurchaseProvider>
}