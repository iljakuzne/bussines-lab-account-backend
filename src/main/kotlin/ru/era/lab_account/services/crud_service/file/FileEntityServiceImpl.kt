package ru.era.lab_account.services.crud_service.file

import io.netty.handler.codec.http.HttpHeaderNames.CONTENT_DISPOSITION
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.HttpHeaders.CONTENT_DISPOSITION
import org.springframework.http.ZeroCopyHttpOutputMessage
import org.springframework.http.codec.multipart.FilePart
import org.springframework.http.server.reactive.ServerHttpResponse
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import reactor.core.publisher.Mono
import ru.era.lab_account.dao.FileProjectRepository
import ru.era.lab_account.domain.FileProject
import ru.era.lab_account.services.file.FileService
import ru.era.lab_account.services.login.HashService
import java.io.File
import java.net.http.HttpHeaders
import java.nio.file.Files
import java.nio.file.Paths


@Service
class FileEntityServiceImpl (private val hashService: HashService,
                             private val fileService: FileService,
                             private val fileProjectRepository: FileProjectRepository,
                             @Value("\${app.filepath}")private val base: String) : FileEntityService {


    @Transactional
    override fun saveFile(file: Mono<FilePart>): Mono<Long> {
        return file
            .flatMap{filePart->
                val fileProject = FileProject(fileName = filePart.filename() ,
                    contentType = getMimeType(filePart.filename()))
                fileProjectRepository.save(fileProject)
            }
            .flatMap {savedFile->
                val stringPath = base + savedFile.id.toString()
                val path = Paths.get(stringPath)
                fileService.uploadFile(file,path)
                    .map {_->hashService.getHashForFile(path)}
                    .flatMap {hash->
                        fileProjectRepository.updateFileProject(hash,stringPath,savedFile.id!!)
                            .map(FileProject::id)
                    }
            }
    }

    @Transactional(readOnly = true)
    override fun downloadFile(fileId: Long, response: ServerHttpResponse) : Mono<Void> {
        return fileProjectRepository.findById(fileId)
            .switchIfEmpty(Mono.error(IllegalArgumentException("Файл ${fileId} не существует")))
            .flatMap {fileProject->
                fileService.downloadFile(fileProject,response)
            }
    }

    private fun getMimeType (fileName: String) : String{
        val localPath = Paths.get(fileName)
        return Files.probeContentType(localPath)?:"text/plain"
    }


}