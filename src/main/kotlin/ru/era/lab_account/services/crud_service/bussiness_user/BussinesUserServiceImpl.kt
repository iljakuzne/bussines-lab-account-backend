package ru.era.lab_account.services.crud_service.bussiness_user

import org.modelmapper.ModelMapper
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import reactor.core.publisher.Flux
import ru.era.lab_account.dao.BussinessUserRepository
import ru.era.lab_account.dto.user.UserDto

@Service
class BussinesUserServiceImpl(private val modelMapper: ModelMapper,
                              private val bussinessUserRepository: BussinessUserRepository) : BussinesUserService {

    @Transactional(readOnly = true)
    override fun findAllUsers(): Flux<UserDto> {
        return bussinessUserRepository.findAll()
            .map {bussinesUser->
                modelMapper.map(bussinesUser,UserDto::class.java)
            }
    }
}