package ru.era.lab_account.services.crud_service.region

import reactor.core.publisher.Flux
import ru.era.lab_account.dto.helper.KeyValueDto

interface RegionService {

    fun findAllRegions () : Flux<KeyValueDto>
}