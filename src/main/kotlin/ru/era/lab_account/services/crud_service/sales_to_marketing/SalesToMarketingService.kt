package ru.era.lab_account.services.crud_service.sales_to_marketing

import reactor.core.publisher.Flux
import ru.era.lab_account.domain.SalesToMarketing

interface SalesToMarketingService {

    fun createSalesToMarket (marketindId: Long, salesIds: List<Long>) : Flux<SalesToMarketing>
}