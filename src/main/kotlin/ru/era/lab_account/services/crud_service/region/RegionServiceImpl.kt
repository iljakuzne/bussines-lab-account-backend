package ru.era.lab_account.services.crud_service.region

import org.modelmapper.ModelMapper
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import reactor.core.publisher.Flux
import ru.era.lab_account.dao.RegionRepository
import ru.era.lab_account.dto.helper.KeyValueDto

@Service
class RegionServiceImpl(private val modelMapper: ModelMapper,
                        private val regionRepository: RegionRepository) : RegionService {

    @Transactional(readOnly = true)
    override fun findAllRegions(): Flux<KeyValueDto> {
        return regionRepository.findAll()
            .map {region->modelMapper.map(region,KeyValueDto::class.java)  }
    }
}