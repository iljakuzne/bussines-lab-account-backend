package ru.era.lab_account.services.crud_service.sales_to_marketing

import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import ru.era.lab_account.dao.SalesToMarketingRepository
import ru.era.lab_account.domain.SalesToMarketing

@Service
class SalesToMarketingServiceImpl (private val salesToMarketingRepository: SalesToMarketingRepository)
    : SalesToMarketingService{


    @Transactional
    override fun createSalesToMarket(marketindId: Long, salesIds: List<Long>): Flux<SalesToMarketing> {
        return Mono.fromCallable {
            salesIds.map {saleId-> SalesToMarketing(marketingId = marketindId,
                typeSales=saleId) }
        }.flatMapMany {entityList->
            salesToMarketingRepository.saveAll(entityList)
        }
    }
}