package ru.era.lab_account.services.crud_service.mark

import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import ru.era.lab_account.domain.view.ProjectForMark
import ru.era.lab_account.dto.mark.CreateMarkDto
import ru.era.lab_account.dto.mark.MarkDto

interface MarkService {

    fun createMark (dto: CreateMarkDto) : Mono<Long>
    /**
     * Проверка перехода проекта на этап 2
     */
    fun checkMoveStep (project: ProjectForMark) : Mono<Void>

    fun markForProject (projectId: Long) : Flux<MarkDto>
}