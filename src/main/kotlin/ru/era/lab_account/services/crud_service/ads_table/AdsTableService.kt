package ru.era.lab_account.services.crud_service.ads_table

import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import ru.era.lab_account.dto.ads.AdsTableDto
import ru.era.lab_account.dto.ads.AdsTableDtoToUser
import ru.era.lab_account.dto.ads.CreateAdsDto
import ru.era.lab_account.models.pagination.PaginationRequest

interface AdsTableService {
    /**
     * Создание объявления
     */
    fun createAds (createAdsDto: CreateAdsDto) : Mono<Long>

    fun getAllAds (paginationRequest: PaginationRequest) : Flux<AdsTableDto>

    fun deleteAds (adId: Long) : Mono<Long>

    fun getAdsByUser (paginationRequest: PaginationRequest) : Flux<AdsTableDto>
}