package ru.era.lab_account.services.crud_service.marketing

import reactor.core.publisher.Mono
import ru.era.lab_account.dto.marketing.CreateMarketingDto

interface MarketingService {

    fun createMarketing (dto: CreateMarketingDto) : Mono<Long>
}