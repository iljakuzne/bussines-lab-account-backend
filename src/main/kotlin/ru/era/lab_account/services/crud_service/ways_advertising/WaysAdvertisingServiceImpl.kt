package ru.era.lab_account.services.crud_service.ways_advertising

import org.modelmapper.ModelMapper
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import reactor.core.publisher.Flux
import ru.era.lab_account.dao.WaysAdvertisingRepository
import ru.era.lab_account.dto.helper.KeyValueDto

@Service
class WaysAdvertisingServiceImpl(private val modelMapper: ModelMapper,
                                 private val waysAdvertisingRepository: WaysAdvertisingRepository) : WaysAdvertisingService {

    @Transactional(readOnly = true)
    override fun findAll(): Flux<KeyValueDto> {
        return waysAdvertisingRepository.findAll()
            .map{ways->modelMapper.map(ways,KeyValueDto::class.java)}
    }
}