package ru.era.lab_account.services.crud_service.marketing

import org.modelmapper.ModelMapper
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import reactor.core.publisher.Mono
import reactor.core.publisher.SynchronousSink
import ru.era.lab_account.dao.MarketingTemplateRepository
import ru.era.lab_account.dao.ProductTemplateRepository
import ru.era.lab_account.dao.ProjectTemplateRepository
import ru.era.lab_account.domain.MarketingTemplate
import ru.era.lab_account.domain.ProductTemplate
import ru.era.lab_account.domain.view.ProjectForMark
import ru.era.lab_account.dto.marketing.CreateMarketingDto
import ru.era.lab_account.models.project.ProjectStatus
import ru.era.lab_account.services.crud_service.sales_to_marketing.SalesToMarketingService
import ru.era.lab_account.services.crud_service.ways_advertisment_to_marketing.WaysAdvertismentToMarketingService

@Service
class MarketingServiceImpl (private val modelMapper: ModelMapper,
                            private val salesToMarketingService: SalesToMarketingService,
                            private val marketingTemplateRepository: MarketingTemplateRepository,
                            private val waysAdvertismentToMarketingService: WaysAdvertismentToMarketingService,
                            private val productTemplateRepository: ProductTemplateRepository,
                            private val projectTemplateRepository: ProjectTemplateRepository) : MarketingService {

    @Transactional
    override fun createMarketing(dto: CreateMarketingDto): Mono<Long> {
        return validateCreateMarketing(dto)
            .flatMap {productTemplate->
                val marketing = modelMapper.map(dto,MarketingTemplate::class.java)
                marketingTemplateRepository.save(marketing)
                    .flatMap {savedMarketing->
                        salesToMarketingService.createSalesToMarket(savedMarketing.id!!,dto.typeSales)
                            .thenMany(waysAdvertismentToMarketingService.createWaysAdvertisment(savedMarketing.id!!,dto.advertisingWays))
                            .then(Mono.just(savedMarketing.id!!))
                    }
            }
    }

    @Transactional(readOnly = true)
    protected fun validateCreateMarketing (dto: CreateMarketingDto) : Mono<ProjectForMark>{
        return projectTemplateRepository.getProjectForMark(dto.projectId)
            .switchIfEmpty(Mono.error(IllegalArgumentException("Продукт ${dto.projectId} не существует")))
            .flatMap {project->
                projectTemplateRepository.getProjectForMark(project.projectId)
                    .handle { project, synchronousSink: SynchronousSink<ProjectForMark> ->
                        if (project.status == ProjectStatus.MARKET){
                            synchronousSink.next(project)
                        } else {
                            synchronousSink.error(IllegalArgumentException("Проект не находится на 3 этапе"))
                        }
                    }
            }
    }
}