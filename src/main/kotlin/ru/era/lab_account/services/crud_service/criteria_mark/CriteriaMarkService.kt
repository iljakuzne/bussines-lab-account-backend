package ru.era.lab_account.services.crud_service.criteria_mark

import reactor.core.publisher.Flux
import ru.era.lab_account.dto.helper.KeyValueDto

interface CriteriaMarkService {

    fun getCriteriaByStatus (status: Int) : Flux<KeyValueDto>
}