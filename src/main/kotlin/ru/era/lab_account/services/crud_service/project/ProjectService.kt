package ru.era.lab_account.services.crud_service.project

import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import ru.era.lab_account.dto.product_with_project.ProductWithProjectDto
import ru.era.lab_account.dto.project.CreateProjectDto
import ru.era.lab_account.dto.project.ProjectCardDto
import ru.era.lab_account.domain.view.ProjectCardRating
import ru.era.lab_account.dto.project.ProjectRatingDto
import ru.era.lab_account.models.pagination.PaginationRequest
import ru.era.lab_account.models.project.CreateProjectWithUserId

interface ProjectService {

    fun createProjectTemplate (dto: CreateProjectWithUserId) : Mono<Long>

    fun findProjectsForCard (paginationRequest: PaginationRequest) : Flux<ProjectCardDto>

    fun findProjectWithProductV2 (projectId: Long) : Mono<ProductWithProjectDto>

    fun findProjectWithProductV1 (projectId: Long) : Mono<ProjectCardDto>

    fun findProjectForUser (paginationRequest: PaginationRequest) : Flux<ProjectCardDto>

    fun findProjectForStateholder (paginationRequest: PaginationRequest) : Flux<ProjectCardDto>

    fun findProjectById (projectId: Long) : Mono<CreateProjectDto>

    fun findProjectsWithRating () : Flux<ProjectRatingDto>
}