package ru.era.lab_account.services.crud_service.criteria_mark

import org.modelmapper.ModelMapper
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import reactor.core.publisher.Flux
import ru.era.lab_account.dao.MarkCriteriaRepository
import ru.era.lab_account.dto.helper.KeyValueDto
import ru.era.lab_account.models.project.ProjectStatus

@Service
class CriteriaMarkServiceImpl (private val modelMapper: ModelMapper,
                               private val markCriteriaRepository: MarkCriteriaRepository) : CriteriaMarkService {

    @Transactional(readOnly = true)
    override fun getCriteriaByStatus(status: Int): Flux<KeyValueDto> {
        val projectStatus = ProjectStatus.findByNumber(status)
        return projectStatus?.let {projectStatus ->
            markCriteriaRepository.getCriteriaMarkByStatus(projectStatus)
                .map { markCriteria->
                    modelMapper.map(markCriteria,KeyValueDto::class.java)
                }
        }?:Flux.error<KeyValueDto>(IllegalArgumentException("Статус ${status} не существует"))
    }
}