package ru.era.lab_account.services.crud_service.production

import org.modelmapper.ModelMapper
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import reactor.core.publisher.Mono
import reactor.core.publisher.SynchronousSink
import ru.era.lab_account.dao.ProductionTemplateRepository
import ru.era.lab_account.dao.ProjectTemplateRepository
import ru.era.lab_account.dao.PurchaseProviderRepository
import ru.era.lab_account.domain.ProductionTemplate
import ru.era.lab_account.domain.view.ProjectForMark
import ru.era.lab_account.dto.production.CreateProductionDto
import ru.era.lab_account.models.project.ProjectStatus
import ru.era.lab_account.services.crud_service.purchase_equipment.PurchaseEquipmentService
import ru.era.lab_account.services.crud_service.purchase_provider.PurchaseProviderService
import ru.era.lab_account.services.crud_service.purchases_property.PurchasesPropertyService

@Service
class ProductionServiceImpl (private val modelMapper: ModelMapper,
                             private val purchasesPropertyService: PurchasesPropertyService,
                             private val purchasesProviderService: PurchaseProviderService,
                             private val purchaseEquipmentService: PurchaseEquipmentService,
                             private val productionTemplateRepository: ProductionTemplateRepository,
                             private val projectTemplateRepository: ProjectTemplateRepository) : ProductionService {

    @Transactional
    override fun createProduction(dto: CreateProductionDto): Mono<Long> {
        return validateCreateProduction(dto)
            .flatMap {projectForMark->
                val production = modelMapper.map(dto,ProductionTemplate::class.java)
                productionTemplateRepository.save(production)
                    .flatMap {savedProduction->
                        purchaseEquipmentService.createPurchaseEquipment(savedProduction.id!!,dto.createPurchaseEquimentDto)
                            .thenMany(purchasesPropertyService.createPurchasesProperty(savedProduction.id!!,dto.createPurchasePropertyDto))
                            .thenMany(purchasesProviderService.createPurchaseProviders(savedProduction.id!!,dto.creeatePurchaseProviderDto))
                            .then(Mono.just(savedProduction.id))
                    }
            }
    }

    @Transactional(readOnly = true)
    protected fun validateCreateProduction (dto: CreateProductionDto) : Mono<ProjectForMark> {
        return projectTemplateRepository.getProjectForMark(dto.projectId)
            .switchIfEmpty(Mono.error(IllegalArgumentException("Проект ${dto.projectId} не существует")))
            .handle {project, synchronousSink : SynchronousSink<ProjectForMark>->
                if (project.status == ProjectStatus.MARKET){
                    synchronousSink.next(project)
                } else {
                    synchronousSink.error(IllegalArgumentException("Проект не в 3 стадии"))
                }
            }
    }
}