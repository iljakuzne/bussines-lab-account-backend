package ru.era.lab_account.services.crud_service.type_sales

import org.modelmapper.ModelMapper
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import reactor.core.publisher.Flux
import ru.era.lab_account.dao.TypeSalesRepository
import ru.era.lab_account.dto.helper.KeyValueDto

@Service
class TypeSalesServiceImpl (private val modelMapper: ModelMapper,
                            private val typesSalesRepository: TypeSalesRepository) : TypeSalesService {

    @Transactional(readOnly = true)
    override fun findAll(): Flux<KeyValueDto> {
        return typesSalesRepository.findAll()
            .map{typeSale->modelMapper.map(typeSale,KeyValueDto::class.java)}
    }
}