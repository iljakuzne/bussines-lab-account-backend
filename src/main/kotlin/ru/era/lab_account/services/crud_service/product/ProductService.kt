package ru.era.lab_account.services.crud_service.product

import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import ru.era.lab_account.dto.product.CreateProductDto
import ru.era.lab_account.dto.product.ProductCardDto
import ru.era.lab_account.dto.product_with_project.ProductWithProjectDto
import ru.era.lab_account.dto.project.CreateProjectDto
import ru.era.lab_account.models.pagination.PaginationRequest

interface ProductService {

    fun createProduct (createProjectDto: CreateProductDto) : Mono<Long>

    fun findActualProduct (request: PaginationRequest) : Flux<ProductCardDto>

    fun getById (productId: Long) :Mono<CreateProductDto>

    fun getByProject (projectId: Long) : Mono<CreateProductDto>
}