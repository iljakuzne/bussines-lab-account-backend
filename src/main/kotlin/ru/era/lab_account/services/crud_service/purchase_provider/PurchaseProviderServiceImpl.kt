package ru.era.lab_account.services.crud_service.purchase_provider

import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import ru.era.lab_account.dao.PurchaseProviderRepository
import ru.era.lab_account.domain.PurchaseProvider
import ru.era.lab_account.dto.purchase_provider.CreatePurchaseProviderDto

@Service
class PurchaseProviderServiceImpl(private val purchasesProviderRepository: PurchaseProviderRepository)
    : PurchaseProviderService {

    @Transactional
    override fun createPurchaseProviders(productionId: Long,
        providers: List<CreatePurchaseProviderDto>): Flux<PurchaseProvider> {
        return Mono.fromCallable {
            providers.map { provider->
                PurchaseProvider(productionId = productionId,
                    name = provider.name,
                    resources = provider.resources)
            }
        }.flatMapMany {purchases->
            purchasesProviderRepository.saveAll(purchases)
        }
    }
}