package ru.era.lab_account.services.crud_service.stage_realization

import org.modelmapper.ModelMapper
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import reactor.core.publisher.Flux
import ru.era.lab_account.dao.StageRealizationRepository
import ru.era.lab_account.dto.helper.KeyValueDto

@Service
class StageRealizationServiceImpl (private val modelMapper: ModelMapper,
                                   private val stageRealizationRepository: StageRealizationRepository) : StageRealizationService {

    @Transactional(readOnly = true)
    override fun findAllStageRealization(): Flux<KeyValueDto> {
        return stageRealizationRepository.findAll()
            .map {stage->modelMapper.map(stage,KeyValueDto::class.java) }
    }
}