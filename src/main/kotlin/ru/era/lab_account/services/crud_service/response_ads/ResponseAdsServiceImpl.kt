package ru.era.lab_account.services.crud_service.response_ads

import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import reactor.core.publisher.SynchronousSink
import ru.era.lab_account.dao.AdsTableRepository
import ru.era.lab_account.dao.BussinessUserRepository
import ru.era.lab_account.dao.ResponseAdsRepository
import ru.era.lab_account.domain.AdsTable
import ru.era.lab_account.domain.BussinesUser
import ru.era.lab_account.domain.ResponseAds
import ru.era.lab_account.dto.ads_response.AdsResponseDto
import ru.era.lab_account.services.crud_service.user_principal.UserTokenDecoder

@Service
class ResponseAdsServiceImpl (private val decoder: UserTokenDecoder,
                              private val responseAdsRepository: ResponseAdsRepository,
                              private val bussinessUserRepository: BussinessUserRepository,
                              private val adsRepository: AdsTableRepository) : ResponseAdsService {

    @Transactional
    override fun createResponseAds(adsId: Long): Mono<Long> {
        return validateCreate(adsId)
            .flatMap {userId->
                val a= ResponseAds(userId =userId ,adsId = adsId)
                responseAdsRepository.save(a)
                    .map(ResponseAds::id)
            }
    }

    @Transactional(readOnly = true)
    override fun getResponseForAds(): Flux<AdsResponseDto> {
        return responseAdsRepository.findAll()
            .collectList()
            .flatMapMany {responseAds->
                if (responseAds.isNotEmpty()){
                    val userIds = responseAds.map(ResponseAds::userId)
                    val adsIds = responseAds.map(ResponseAds::adsId)
                    Mono.zip(
                        bussinessUserRepository.findUserByIds(userIds).collectMap(BussinesUser::id),
                        adsRepository.findByIds(adsIds).collectMap(AdsTable::id)
                    )
                        .flatMapMany {tuple->
                            Flux.fromIterable(
                                responseAds.map{response->
                                    val user = tuple.t1[response.userId]!!
                                    val ads = tuple.t2[response.adsId]!!
                                    AdsResponseDto(adsId = ads.id!!,
                                        userId = user.id!!,
                                        userName = user.login,
                                        userPhone = user.phone?:"",
                                        email = user.email,
                                        adsName = ads.name)
                                }
                            )
                        }
                } else {
                    Flux.empty()
                }
            }

    }

    @Transactional(readOnly = true)
    protected fun validateCreate (adsId: Long) : Mono<Long>{
        return decoder.getUserIdByToken()
            .flatMap {userId->
                adsRepository.existsById(adsId)
                    .handle { exists, synchronousSink: SynchronousSink<Boolean> ->
                        if (exists){
                            synchronousSink.next(exists)
                        } else {
                            synchronousSink.error(IllegalArgumentException(""))
                        }
                    }
                    .flatMap {_->
                        responseAdsRepository.existsByUserAndAds(userId,adsId)
                    }
                    .handle { exists, synchronousSink: SynchronousSink<Long> ->
                        if (exists){
                            synchronousSink.error(IllegalArgumentException(""))
                        } else {
                            synchronousSink.next(userId)
                        }
                    }
            }
    }

}