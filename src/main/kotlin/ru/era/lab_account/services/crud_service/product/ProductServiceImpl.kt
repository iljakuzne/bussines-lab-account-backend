package ru.era.lab_account.services.crud_service.product

import org.modelmapper.ModelMapper
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import reactor.core.publisher.SynchronousSink
import ru.era.lab_account.dao.BussinessUserRepository
import ru.era.lab_account.dao.ProductTemplateRepository
import ru.era.lab_account.dao.ProjectTemplateRepository
import ru.era.lab_account.dao.UsingProductRepository
import ru.era.lab_account.domain.ProductTemplate
import ru.era.lab_account.domain.UsingProduct
import ru.era.lab_account.dto.product.CreateProductDto
import ru.era.lab_account.dto.product.ProductCardDto
import ru.era.lab_account.dto.product_with_project.ProductWithProjectDto
import ru.era.lab_account.models.pagination.PaginationRequest
import ru.era.lab_account.models.product.ProductWithUsingNameModel
import ru.era.lab_account.models.product_with_project.ProductWithProjectModel
import ru.era.lab_account.services.crud_service.user_principal.UserTokenDecoder


@Service
class ProductServiceImpl (private val modelMapper: ModelMapper,
                          private val userTokenDecoder: UserTokenDecoder,
                          private val projectTemplateRepository: ProjectTemplateRepository,
                          private val productTemplateRepository: ProductTemplateRepository,
                          private val usingProductRepository: UsingProductRepository) : ProductService {

    @Transactional
    override fun createProduct(createProductDto: CreateProductDto): Mono<Long> {
        return validateCreateProduct(createProductDto)
            .flatMap {_->
                val product = modelMapper.map(createProductDto,ProductTemplate::class.java)
                productTemplateRepository.save(product)
            }
            .map(ProductTemplate::id)
    }

    @Transactional(readOnly = true)
    override fun findActualProduct(request: PaginationRequest): Flux<ProductCardDto> {
        return productTemplateRepository.findActualProduct(request.limit,
            request.limit * request.pageCount)
            .collectList()
            .flatMapMany {product->
                val usingIds = product.map(ProductTemplate::usingProductId).distinct()
                usingProductRepository.findByIds(usingIds)
                    .collectMap(UsingProduct::id!!)
                    .flatMapMany {map->
                        Flux.fromIterable(
                            product.map {productTemplate ->
                                val using = map[productTemplate.usingProductId]?.name?:"Не задано"
                                modelMapper.map(ProductWithUsingNameModel(productTemplate,using),ProductCardDto::class.java)
                            }
                        )
                    }
            }
    }

    @Transactional(readOnly = true)
    override fun getById(productId: Long): Mono<CreateProductDto> {
        return productTemplateRepository.findById(productId)
            .switchIfEmpty(Mono.error(IllegalArgumentException("Продукт ${productId} не существует")))
            .map {product->
                modelMapper.map(product,CreateProductDto::class.java)
            }
    }

    @Transactional(readOnly = true)
    override fun getByProject(projectId: Long): Mono<CreateProductDto> {
        return productTemplateRepository.findProductByProjectId(projectId)
            .switchIfEmpty(Mono.error(IllegalArgumentException("Продукт для проекта ${projectId} не существует")))
            .map {product->
                modelMapper.map(product,CreateProductDto::class.java)
            }
    }

    //TODO проверка на доступность создания по оценке или рейтингу
    @Transactional(readOnly = true)
    protected fun validateCreateProduct (createProductDto: CreateProductDto) : Mono<Boolean>{
        return userTokenDecoder.getUserIdByToken()
            .flatMap {userId->
                projectTemplateRepository.existsProjectByIdAndUserId(projectId = createProductDto.projectId,
                    userId = userId)
            }
            .handle { exists, synchronousSink: SynchronousSink<Boolean> ->
                if (exists){
                    synchronousSink.next(exists)
                } else {
                    synchronousSink.error(IllegalArgumentException("Проект ${createProductDto.projectId} принадледит другому пользователю"))
                }
            }
            .flatMap {_->
                productTemplateRepository.existsByProjectId(createProductDto.projectId)
            }
            .handle { exists, synchronousSink: SynchronousSink<Boolean> ->
                if (!exists){
                    synchronousSink.next(exists)
                } else {
                    synchronousSink.error(IllegalArgumentException("В проекте ${createProductDto.projectId} уже существует продукт"))
                }
            }
            .flatMap {_->
                usingProductRepository.existsById(createProductDto.usingProductId)
            }
            .handle{exists, synchronousSink: SynchronousSink<Boolean> ->
                if (exists){
                    synchronousSink.next(exists)
                } else {
                    synchronousSink.error(IllegalArgumentException("Периодичность использования ${createProductDto.usingProductId} не найдена"))
                }
            }
    }

}