package ru.era.lab_account.services.crud_service.competitor

import org.modelmapper.ModelMapper
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import ru.era.lab_account.dao.CompetitorRepository
import ru.era.lab_account.domain.Competitor
import ru.era.lab_account.dto.competitor.CreateCompetitorDto
import ru.era.lab_account.models.competitior.CompetitorWithMarketId

@Service
class CompetitorServiceImpl (private val modelMapper: ModelMapper,
                             private val competitorRepository : CompetitorRepository) : CompetitorService {

    @Transactional
    override fun createCompetitor(competitorsDto: List<CreateCompetitorDto>, marketId: Long): Flux<Competitor> {
        return Mono.fromCallable {

            competitorsDto.map {comp->
                val model = CompetitorWithMarketId(comp,marketId)
                modelMapper.map(model,Competitor::class.java)
            }
        }.flatMapMany {competitors->
            competitorRepository.saveAll(competitors)
        }
    }

    @Transactional(readOnly = true)
    override fun getCompetitorsForMarket(marketId: Long): Flux<CreateCompetitorDto> {
        return competitorRepository.findByMarket(marketId)
            .map { competitor->
                modelMapper.map(competitor,CreateCompetitorDto::class.java)
            }
    }
}