package ru.era.lab_account.services.crud_service.bussiness_user

import reactor.core.publisher.Flux
import ru.era.lab_account.dto.user.UserDto

interface BussinesUserService {

    fun findAllUsers () : Flux<UserDto>
    //fun findMarkedUser (projectId: Long) :
}