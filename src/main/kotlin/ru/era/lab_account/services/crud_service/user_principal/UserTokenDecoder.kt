package ru.era.lab_account.services.crud_service.user_principal

import reactor.core.publisher.Mono

interface UserTokenDecoder {

    fun getUserIdByToken () : Mono<Long>
}