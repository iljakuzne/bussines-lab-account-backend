package ru.era.lab_account.services.crud_service.type_sales

import reactor.core.publisher.Flux
import ru.era.lab_account.dto.helper.KeyValueDto

interface TypeSalesService {

    fun findAll () : Flux<KeyValueDto>


}