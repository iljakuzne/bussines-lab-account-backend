package ru.era.lab_account.services.crud_service.purchase_equipment

import reactor.core.publisher.Flux
import ru.era.lab_account.domain.PurchaseEquipment
import ru.era.lab_account.dto.purchase_equipment.CreatePurchaseEquimentDto


interface PurchaseEquipmentService {

    fun createPurchaseEquipment (productionId: Long, equipment: List<CreatePurchaseEquimentDto>) : Flux<PurchaseEquipment>
}