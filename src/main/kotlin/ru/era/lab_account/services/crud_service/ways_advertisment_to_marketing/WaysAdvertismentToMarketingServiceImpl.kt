package ru.era.lab_account.services.crud_service.ways_advertisment_to_marketing

import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import ru.era.lab_account.dao.WaysAdvertismentToMarketingRepository
import ru.era.lab_account.domain.WaysAdvertismentToMarketing

@Service
class WaysAdvertismentToMarketingServiceImpl (private val waysAdvertisingRepository: WaysAdvertismentToMarketingRepository)
    : WaysAdvertismentToMarketingService {

    @Transactional
    override fun createWaysAdvertisment(marketingId: Long,
                                        waysAdvertismentIds: List<Long>): Flux<WaysAdvertismentToMarketing> {
        return  Mono.fromCallable {
            waysAdvertismentIds.map {ways->
                WaysAdvertismentToMarketing(marketingId = marketingId,
                    waysAdvertisingId = ways)
            }
        }.flatMapMany {ways->
            waysAdvertisingRepository.saveAll(ways)
        }
    }
}