package ru.era.lab_account.services.crud_service.production

import reactor.core.publisher.Mono
import ru.era.lab_account.dto.production.CreateProductionDto

interface ProductionService {

    fun createProduction (dto: CreateProductionDto) : Mono<Long>


}