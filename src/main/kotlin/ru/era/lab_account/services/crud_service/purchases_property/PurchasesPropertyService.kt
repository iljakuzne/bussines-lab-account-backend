package ru.era.lab_account.services.crud_service.purchases_property

import reactor.core.publisher.Flux
import ru.era.lab_account.domain.PurchaseProperty
import ru.era.lab_account.dto.purchase_property.CreatePurchasePropertyDto

interface PurchasesPropertyService {

    fun createPurchasesProperty (productionId: Long, properties: List<CreatePurchasePropertyDto>) : Flux<PurchaseProperty>
}