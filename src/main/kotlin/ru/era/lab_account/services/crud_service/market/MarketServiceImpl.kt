package ru.era.lab_account.services.crud_service.market

import org.modelmapper.ModelMapper
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import reactor.core.publisher.Mono
import ru.era.lab_account.dao.MarketTemplateRepository
import ru.era.lab_account.domain.MarketTemplate
import ru.era.lab_account.dto.market.CreateMarketDto
import ru.era.lab_account.models.market.MarketWithCompetitorsDto
import ru.era.lab_account.services.crud_service.competitor.CompetitorService

@Service
class MarketServiceImpl (private val marketTemplateRepository: MarketTemplateRepository,
                         private val competitorService: CompetitorService,
                         private val modelMapper: ModelMapper) : MarketService {

    @Transactional
    override fun createMarket(dto: CreateMarketDto): Mono<Long> {
        return Mono.fromCallable {
            modelMapper.map(dto,MarketTemplate::class.java)
        }
            .flatMap{market->
                marketTemplateRepository.save(market)
            }
            .flatMap {marketTemplate->
                competitorService.createCompetitor(dto.competitors,marketTemplate.id!!)
                    .then(Mono.just(marketTemplate.id))
            }
    }

    @Transactional(readOnly = true)
    override fun getMarketById(marketId: Long): Mono<CreateMarketDto> {
        return marketTemplateRepository.findById(marketId)
            .switchIfEmpty(Mono.error(IllegalArgumentException("Маркетинговый план ${marketId} не найден")))
            .flatMap{market->
                competitorService.getCompetitorsForMarket(marketId)
                    .collectList()
                    .map {competitors->
                        val model = MarketWithCompetitorsDto(market,competitors)
                        modelMapper.map(model, CreateMarketDto::class.java)
                    }
            }
    }

    @Transactional(readOnly = true)
    override fun getByProjectId(projectId: Long): Mono<CreateMarketDto> {
        return marketTemplateRepository.findByProjectId(projectId)
            .switchIfEmpty(Mono.error(IllegalArgumentException("Маркетинговый план ${projectId} не найден")))
            .flatMap{market->
                competitorService.getCompetitorsForMarket(market.id!!)
                    .collectList()
                    .map {competitors->
                        val model = MarketWithCompetitorsDto(market,competitors)
                        modelMapper.map(model, CreateMarketDto::class.java)
                    }
            }
    }


}