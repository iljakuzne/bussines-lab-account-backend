package ru.era.lab_account.services.crud_service.company

import reactor.core.publisher.Flux
import ru.era.lab_account.dto.helper.KeyValueDto

interface CompanyService {

    fun getAllCompanies () : Flux<KeyValueDto>
}