package ru.era.lab_account.services.crud_service.response_ads

import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import ru.era.lab_account.dto.ads_response.AdsResponseDto

interface ResponseAdsService {

    fun createResponseAds (adsId: Long) : Mono<Long>

    fun getResponseForAds () : Flux<AdsResponseDto>
}