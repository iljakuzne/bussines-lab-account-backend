package ru.era.lab_account.services.crud_service.use_product

import reactor.core.publisher.Flux
import ru.era.lab_account.dto.helper.KeyValueDto

interface UsingProductService {

    fun findAllUsingProduct () : Flux<KeyValueDto>
}