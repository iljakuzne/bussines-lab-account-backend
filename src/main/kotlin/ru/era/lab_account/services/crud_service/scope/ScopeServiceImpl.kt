package ru.era.lab_account.services.crud_service.scope

import org.modelmapper.ModelMapper
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import reactor.core.publisher.Flux
import ru.era.lab_account.dao.ScopeRepository
import ru.era.lab_account.dto.helper.KeyValueDto

@Service
class ScopeServiceImpl (private val scopeRepository: ScopeRepository,
                        private val modelMapper: ModelMapper) : ScopeService {

    @Transactional(readOnly = true)
    override fun findAll(): Flux<KeyValueDto> {
        return scopeRepository.findAll()
            .map {scope->  modelMapper.map(scope,KeyValueDto::class.java)}
    }
}