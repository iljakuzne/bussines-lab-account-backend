package ru.era.lab_account.services.crud_service.crud_helper

import org.springframework.data.r2dbc.repository.R2dbcRepository
import reactor.core.publisher.Mono


interface ExistsCheckEntity {


    fun <Entity> validateExistsId (id : Long, repository: R2dbcRepository<Entity,Long>) : Mono<Boolean>

    fun <Entity> validateNotExistsId (id : Long, repository: R2dbcRepository<Entity,Long>) : Mono<Boolean>

}