package ru.era.lab_account.services.crud_service.ways_advertising

import reactor.core.publisher.Flux
import ru.era.lab_account.dto.helper.KeyValueDto

interface WaysAdvertisingService {

    fun findAll () : Flux<KeyValueDto>
}