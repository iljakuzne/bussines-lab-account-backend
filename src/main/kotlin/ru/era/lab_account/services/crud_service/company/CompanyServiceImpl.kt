package ru.era.lab_account.services.crud_service.company

import org.modelmapper.ModelMapper
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import reactor.core.publisher.Flux
import ru.era.lab_account.dao.CompanyRepository
import ru.era.lab_account.dto.helper.KeyValueDto

@Service
class CompanyServiceImpl (private val companyRepository: CompanyRepository,
                          private val modelMapper: ModelMapper) : CompanyService {

    @Transactional(readOnly = true)
    override fun getAllCompanies(): Flux<KeyValueDto> {
        return companyRepository.findAll()
            .map{company->
                modelMapper.map(company,KeyValueDto::class.java)
            }
    }
}