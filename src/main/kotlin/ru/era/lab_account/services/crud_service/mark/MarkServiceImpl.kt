package ru.era.lab_account.services.crud_service.mark

import org.modelmapper.ModelMapper
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import reactor.core.publisher.SynchronousSink
import ru.era.lab_account.dao.MarkCriteriaRepository
import ru.era.lab_account.dao.MarkRepository
import ru.era.lab_account.dao.ProjectTemplateRepository
import ru.era.lab_account.domain.Mark
import ru.era.lab_account.domain.MarkCriteria
import ru.era.lab_account.domain.view.ProjectForMark
import ru.era.lab_account.dto.mark.CreateMarkDto
import ru.era.lab_account.dto.mark.MarkDto
import ru.era.lab_account.models.mark.MarkWithUserId
import ru.era.lab_account.models.project.ProjectStatus
import ru.era.lab_account.services.crud_service.user_principal.UserTokenDecoder

@Service
class MarkServiceImpl (private val modelMapper: ModelMapper,
                       private val decoder: UserTokenDecoder,
                       private val projectTemplateRepository: ProjectTemplateRepository,
                       private val markCriteriaRepository: MarkCriteriaRepository,
                       private val markRepository: MarkRepository)
    : MarkService {

    @Transactional
    override fun createMark(dto: CreateMarkDto): Mono<Long> {
        return validateCreateMark(dto)
            .flatMap {pair->
                val model = MarkWithUserId(dto,pair.first)
                val mark = modelMapper.map(model, Mark::class.java)
                markRepository.save(mark)
                    .map(Mark::id!!)
                    .doOnSuccess {markId->
                        checkMoveStep(pair.second)
                    }
            }
    }

    @Transactional
    override fun checkMoveStep(project: ProjectForMark): Mono<Void> {
        return markRepository.selectSumMark(project.projectId,project.status)
            .flatMap {sum->
                if (ProjectStatus.checkNextStatus(sum,project.status)){
                    projectTemplateRepository.updateStatus(ProjectStatus.getNextStatus(project.status),project.projectId)
                } else {
                    Mono.empty<Void>()
                }
            }
    }

    @Transactional(readOnly = true)
    override fun markForProject(projectId: Long): Flux<MarkDto> {
        return projectTemplateRepository.getProjectForMark(projectId)
            .switchIfEmpty(Mono.error(IllegalArgumentException("Проект ${projectId} не существует")))
            .flatMapMany {project->
                markRepository.selectSumMarsForProject(project.projectId,project.status)
                    .map{markResultView->
                        modelMapper.map(markResultView,MarkDto::class.java)
                    }
            }
    }


    @Transactional(readOnly = true)
    protected fun validateCreateMark (dto: CreateMarkDto) : Mono<Pair<Long,ProjectForMark>>{
        return if (dto.mark>5 || dto.mark <0){
            Mono.error(IllegalArgumentException("Оценка должна быть от 0 до 5"))
        } else {
            decoder.getUserIdByToken()
                .flatMap { userId ->
                    projectTemplateRepository.getProjectForMark(dto.projectId)
                        .switchIfEmpty(Mono.error(IllegalArgumentException("Проект ${dto.projectId} не существует")))
                        .handle { projectForMark, synchronousSink: SynchronousSink<ProjectForMark> ->
                            if (projectForMark.userId == userId) {
                                synchronousSink.error(IllegalArgumentException("Нельзя проставлять оценку совему проекту"))
                            } else {
                                synchronousSink.next(projectForMark)
                            }
                        }
                        .flatMap { projectForMark ->
                            markCriteriaRepository.findById(dto.criteriaId)
                                .switchIfEmpty(Mono.error(IllegalArgumentException("Критерий оценки ${dto.criteriaId} не существует")))
                                .handle { markCriteria, synchronousSink: SynchronousSink<MarkCriteria> ->
                                    if (markCriteria.criteriaStatus == projectForMark.status) {
                                        synchronousSink.next(markCriteria)
                                    } else {
                                        synchronousSink.error(IllegalArgumentException("Критерий оценки не соответсвует данному этапу"))
                                    }
                                }
                                .flatMap { markCriteria ->
                                    markRepository.existsMarkByCriteriaAndUser(userId, markCriteria.id,projectForMark.projectId)
                                        .handle { exists, synchronousSink: SynchronousSink<Pair<Long,ProjectForMark>> ->
                                            if (exists) {
                                                synchronousSink.error(IllegalArgumentException("Пользователь ${userId} уже оцениквал проект ${dto.projectId}"))
                                            } else {
                                                synchronousSink.next(userId to projectForMark)
                                            }
                                        }
                                }
                        }
                }
        }
    }


}