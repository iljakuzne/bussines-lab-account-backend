package ru.era.lab_account.services.crud_service.market

import reactor.core.publisher.Mono
import ru.era.lab_account.dto.market.CreateMarketDto

interface MarketService {

    fun createMarket (dto: CreateMarketDto) : Mono<Long>

    fun getMarketById (marketId: Long) : Mono<CreateMarketDto>

    fun getByProjectId (productId: Long) : Mono<CreateMarketDto>
}