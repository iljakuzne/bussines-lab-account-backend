package ru.era.lab_account.services.crud_service.stage_realization

import reactor.core.publisher.Flux
import ru.era.lab_account.dto.helper.KeyValueDto

interface StageRealizationService {

    fun findAllStageRealization () : Flux<KeyValueDto>
}