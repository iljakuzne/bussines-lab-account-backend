package ru.era.lab_account.services.crud_service.ways_advertisment_to_marketing

import reactor.core.publisher.Flux
import ru.era.lab_account.domain.WaysAdvertismentToMarketing

interface WaysAdvertismentToMarketingService {

    fun createWaysAdvertisment (marketingId: Long,
                                waysAdvertismentIds: List<Long>)
            : Flux<WaysAdvertismentToMarketing>
}