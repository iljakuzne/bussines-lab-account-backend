package ru.era.lab_account.services.crud_service.use_product

import org.modelmapper.ModelMapper
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import reactor.core.publisher.Flux
import ru.era.lab_account.dao.UsingProductRepository
import ru.era.lab_account.dto.helper.KeyValueDto

@Service
class UsingProductServiceImpl(private val modelMapper: ModelMapper,
                              private val usingProductRepository: UsingProductRepository) : UsingProductService {

    @Transactional(readOnly = true)
    override fun findAllUsingProduct(): Flux<KeyValueDto> {
        return usingProductRepository.findAll()
            .map {usingProd-> modelMapper.map(usingProd,KeyValueDto::class.java) }
    }
}