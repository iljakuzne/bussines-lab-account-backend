package ru.era.lab_account.services.crud_service.user_principal

import io.jsonwebtoken.Claims
import org.springframework.security.core.context.ReactiveSecurityContextHolder
import org.springframework.stereotype.Service
import reactor.core.publisher.Mono

@Service
class UserTokenDecoderImpl : UserTokenDecoder {

    override fun getUserIdByToken(): Mono<Long> {
        return ReactiveSecurityContextHolder.getContext()
            .map {context->
                 (context.authentication.principal as Claims)["user_id",Integer::class.java]!!.toLong()
            }
    }
}