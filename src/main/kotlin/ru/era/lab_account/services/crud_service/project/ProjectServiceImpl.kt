package ru.era.lab_account.services.crud_service.project

import org.modelmapper.ModelMapper
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import ru.era.lab_account.dao.*
import ru.era.lab_account.domain.BussinesUser
import ru.era.lab_account.domain.ProjectTemplate
import ru.era.lab_account.domain.Scope
import ru.era.lab_account.dto.product_with_project.ProductWithProjectDto
import ru.era.lab_account.dto.project.CreateProjectDto
import ru.era.lab_account.dto.project.ProjectCardDto
import ru.era.lab_account.domain.view.ProjectCardRating
import ru.era.lab_account.dto.project.ProjectRatingDto
import ru.era.lab_account.models.pagination.PaginationRequest
import ru.era.lab_account.models.product_with_project.ProductWithProjectModel
import ru.era.lab_account.models.project.CardProjectModel
import ru.era.lab_account.models.project.CreateProjectWithUserId
import ru.era.lab_account.services.crud_service.crud_helper.ExistsCheckEntity
import ru.era.lab_account.services.crud_service.user_principal.UserTokenDecoder

@Service
class ProjectServiceImpl(private val decoder: UserTokenDecoder,
                         private val userRepository: BussinessUserRepository,
                         private val productTemplateRepository: ProductTemplateRepository,
                         private val scopeRepository: ScopeRepository,
                         private val stageRealizationRepository: StageRealizationRepository,
                         private val existsCheckEntity: ExistsCheckEntity,
                         private val modelMapper: ModelMapper,
                         private val projectTemplateRepository: ProjectTemplateRepository) : ProjectService {

    @Transactional
    override fun createProjectTemplate(dto: CreateProjectWithUserId): Mono<Long> {
       return validateCreate(dto)
           .map {_->modelMapper.map(dto,ProjectTemplate::class.java)}
           .flatMap {project->
               projectTemplateRepository.save(project)
           }
           .map(ProjectTemplate::id!!)
    }

    @Transactional(readOnly = true)
    override fun findProjectsForCard(paginationRequest: PaginationRequest): Flux<ProjectCardDto> {
        return projectTemplateRepository.findAllProjectByPagination(paginationRequest.limit,
            paginationRequest.pageCount)
            .collectList()
            .flatMapMany(::handlePaginationProjectCard)
    }

    @Transactional(readOnly = true)
    override fun findProjectWithProductV2(projectId: Long): Mono<ProductWithProjectDto> {
        return findProject(projectId)
            .flatMap {pair->
                productTemplateRepository.findProductByProjectId(pair.first.id!!)
                    .map {product->
                        val model = ProductWithProjectModel(pair.first,product,pair.second)
                        modelMapper.map(model,ProductWithProjectDto::class.java)
                    }
            }
    }

    @Transactional(readOnly = true)
    override fun findProjectWithProductV1(projectId: Long): Mono<ProjectCardDto> {
        return findProject(projectId)
            .flatMap {pair->
                scopeRepository.findById(pair.first.scopeId)
                    .map {scope->
                        modelMapper.map(CardProjectModel(pair.first,scope.name,pair.second),ProjectCardDto::class.java)
                    }
            }
    }

    @Transactional(readOnly = true)
    override fun findProjectForUser(paginationRequest: PaginationRequest): Flux<ProjectCardDto> {
        return decoder.getUserIdByToken()
            .flatMapMany { userId ->
                projectTemplateRepository.getProjectsForUser(
                    userId,
                    paginationRequest.limit,
                    paginationRequest.pageCount
                )
                    .collectList()
                    .flatMapMany(::handlePaginationProjectCard)
            }
    }

    @Transactional(readOnly = true)
    override fun findProjectForStateholder(paginationRequest: PaginationRequest): Flux<ProjectCardDto> {
        return decoder.getUserIdByToken()
            .flatMapMany {userId->
                projectTemplateRepository.findProjectForStateholder(userId,
                    paginationRequest.limit,
                    paginationRequest.pageCount)
                    .collectList()
                    .flatMapMany(::handlePaginationProjectCard)
            }
    }

    @Transactional(readOnly = true)
    override fun findProjectById(projectId: Long): Mono<CreateProjectDto> {
        return projectTemplateRepository.findById(projectId)
            .switchIfEmpty(Mono.error(IllegalArgumentException("Проект ${projectId} не найден")))
            .map {project->
                modelMapper.map(project,CreateProjectDto::class.java)
            }
    }

    @Transactional(readOnly = true)
    override fun findProjectsWithRating(): Flux<ProjectRatingDto> {
        return projectTemplateRepository.getAllProjectWithRating()
            .map{rating->
                modelMapper.map(rating,ProjectRatingDto::class.java)
            }
    }

    @Transactional(readOnly = true)
    protected fun handlePaginationProjectCard (projects: List<ProjectTemplate>) : Flux<ProjectCardDto>{
        return if (projects.isNotEmpty()){
            val userIds = projects.map(ProjectTemplate::userId).distinct()
            val scopeIds = projects.map(ProjectTemplate::scopeId).distinct()
            Mono.zip(userRepository.findUserByIds(userIds).collectMap(BussinesUser::id),
                scopeRepository.findScopeByIds(scopeIds).collectMap(Scope::id!!))
                .flatMapMany {tuple->
                    Flux.fromIterable(
                        projects.map {project->
                            val user = tuple.t1[project.userId]!!
                            val scope = tuple.t2[project.scopeId]?.name?:"Scope не задан"
                            modelMapper.map(CardProjectModel(project,scope,user),ProjectCardDto::class.java)
                        })
                }
        } else {
            Flux.empty()
        }
    }

    @Transactional(readOnly = true)
    protected fun findProject (projectId: Long) : Mono<Pair<ProjectTemplate,BussinesUser>> {
        return projectTemplateRepository.findById(projectId)
            .switchIfEmpty(Mono.error(IllegalArgumentException("Проект с идентификатором ${projectId} не существует")))
            .flatMap {project->
                userRepository.findById(project.userId)
                    .map {user->
                        project to user
                    }
            }
    }

    @Transactional(readOnly = true)
    protected fun validateCreate (dto: CreateProjectWithUserId) : Mono<Boolean>{
        return existsCheckEntity.validateExistsId(dto.userId,userRepository)
            .flatMap {_->existsCheckEntity.validateExistsId(dto.project.stageId,stageRealizationRepository)}
            .flatMap {_->existsCheckEntity.validateExistsId(dto.project.scopeId,scopeRepository) }
    }
}