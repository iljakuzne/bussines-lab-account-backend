package ru.era.lab_account.services.crud_service.competitor

import reactor.core.publisher.Flux
import ru.era.lab_account.domain.Competitor
import ru.era.lab_account.dto.competitor.CreateCompetitorDto

interface CompetitorService {

    fun createCompetitor (competitorsDto: List<CreateCompetitorDto>, marketId: Long) : Flux<Competitor>

    fun getCompetitorsForMarket (marketId: Long) : Flux<CreateCompetitorDto>
}