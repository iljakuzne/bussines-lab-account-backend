package ru.era.lab_account.services.registration

import org.modelmapper.ModelMapper
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import reactor.core.publisher.Mono
import reactor.core.publisher.SynchronousSink
import ru.era.lab_account.dao.BussinessUserRepository
import ru.era.lab_account.dao.CompanyRepository
import ru.era.lab_account.domain.BussinesUser
import ru.era.lab_account.dto.auth.RegistrationDto

@Service
class RegistrationServiceImpl(private val modelMapper: ModelMapper,
                              private val companyRepository: CompanyRepository,
                              private val bussinessUserRepository: BussinessUserRepository) : RegistrationService {

    @Transactional
    override fun createNewUser(model: RegistrationDto): Mono<Long> {
        return validateCreateUser(model)
            .flatMap {exists->
                val bussinesUser = modelMapper.map(model,BussinesUser::class.java)
                bussinessUserRepository.save(bussinesUser)
            }
            .map(BussinesUser::id!!)
    }


    @Transactional(readOnly = true)
    protected fun validateCreateUser (model: RegistrationDto) : Mono<Boolean>{
        return bussinessUserRepository.existsUserByLogin(model.login)
            .handle{exists, synchronousSink: SynchronousSink<Boolean> ->
                if (exists){
                    synchronousSink.error(IllegalArgumentException("Пользователь с логином ${model.login} уже существует"))
                } else {
                    synchronousSink.next(exists)
                }
            }.flatMap {exists->
                companyRepository.existsById(model.companyId)
            }
            .handle{exists,synchronousSink: SynchronousSink<Boolean>->
                if (!exists){
                    synchronousSink.error(IllegalArgumentException("Компания с идентификатором ${model.companyId} не существует"))
                } else {
                    synchronousSink.next(exists)
                }
            }
    }
}