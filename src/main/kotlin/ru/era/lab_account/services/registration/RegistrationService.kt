package ru.era.lab_account.services.registration

import reactor.core.publisher.Mono
import ru.era.lab_account.dto.auth.RegistrationDto

interface RegistrationService {

    fun createNewUser (model: RegistrationDto) : Mono<Long>


}