package ru.era.lab_account.dao

import org.springframework.data.r2dbc.repository.Query
import org.springframework.data.r2dbc.repository.R2dbcRepository
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import ru.era.lab_account.domain.Mark
import ru.era.lab_account.domain.view.MarkResultView
import ru.era.lab_account.models.project.ProjectStatus

@Repository
interface MarkRepository : R2dbcRepository<Mark,Long> {

    @Query("""
        select exists (
            select id 
            from mark
            where user_id = :userId
            AND mark_criteria = :criteriaMarkId
            AND project_id = :projectId
        )
    """)
    fun existsMarkByCriteriaAndUser (@Param("userId") userId: Long,
                                     @Param("criteriaMarkId") criteriaMarkId: Long,
                                     @Param("projectId") projectId: Long) : Mono<Boolean>

    @Query("""
        WITH criteria_id AS (
            select id 
            from mark_criteria
            where criteria_status = :criteriaStatus
        )
        select  coalesce(SUM (mark),0)
                from mark
                where
                project_id = :projectId
                AND mark_criteria IN (select * from criteria_id)
    """)
    fun selectSumMark (@Param("projectId") projectId: Long,
                       @Param("criteriaStatus") criteriaStatus: ProjectStatus) : Mono<Int>


    @Query("""
        WITH criterion as (
            select * 
            from  mark_criteria
            where criteria_status = :status
        )
        
        
        select c.id, c.criteria_name, coalesce(SUM(m.mark),0) as summa
        from criterion as c left join mark as m ON c.id = m.mark_criteria
        and project_id = :projectId
        group by c.id, c.criteria_name
    """)
    fun selectSumMarsForProject (@Param("projectId") projectId: Long,
                                 @Param("status") status: ProjectStatus) : Flux<MarkResultView>
}