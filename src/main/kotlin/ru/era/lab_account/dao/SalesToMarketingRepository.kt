package ru.era.lab_account.dao

import org.springframework.data.r2dbc.repository.R2dbcRepository
import org.springframework.stereotype.Repository
import ru.era.lab_account.domain.SalesToMarketing

@Repository
interface SalesToMarketingRepository : R2dbcRepository<SalesToMarketing,Long> {
}