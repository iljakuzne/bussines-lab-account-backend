package ru.era.lab_account.dao

import org.springframework.data.r2dbc.repository.Query
import org.springframework.data.r2dbc.repository.R2dbcRepository
import org.springframework.stereotype.Repository
import reactor.core.publisher.Flux
import ru.era.lab_account.domain.Scope
import ru.era.lab_account.domain.TypeSales

@Repository
interface TypeSalesRepository : R2dbcRepository<TypeSales, Long> {

    @Query("""
        select * 
        from type_sales
        where id IN (:ids)
    """)
    fun findTypeSalesByIds (ids: List<Long>) : Flux<TypeSales>
}