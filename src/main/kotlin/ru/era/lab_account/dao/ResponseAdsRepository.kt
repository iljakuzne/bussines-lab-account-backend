package ru.era.lab_account.dao

import org.springframework.data.r2dbc.repository.Query
import org.springframework.data.r2dbc.repository.R2dbcRepository
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import ru.era.lab_account.domain.ResponseAds

@Repository
interface ResponseAdsRepository : R2dbcRepository<ResponseAds,Long> {

    @Query("""
        select exists (select id from response_ads where user_id = :userId AND ads_id = :adsId)
    """)
    fun existsByUserAndAds (@Param("userId") userId: Long,
                            @Param("adsId") adsId: Long) : Mono<Boolean>

    @Query("""
        select *
        from response_ads
        where ads_id = :adsId
    """)
    fun getResponseForAds (@Param ("adsId") adsId: Long) : Flux<ResponseAds>
}