package ru.era.lab_account.dao

import org.springframework.data.r2dbc.repository.Query
import org.springframework.data.r2dbc.repository.R2dbcRepository
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import ru.era.lab_account.domain.AdsTable

@Repository
interface AdsTableRepository : R2dbcRepository<AdsTable,Long> {

    @Query("""
        select * 
        from ads
        ORDER BY created_at DESC
        limit :limit
        offset :offset
    """)
    fun paginationFindAll (@Param("limit")limit: Int,
                           @Param("offset") offset: Int) : Flux<AdsTable>

    @Query("""
        select exists (select id from ads where id = :adId and user_id = :userId)
    """)
    fun existsAdsByUserAndId (userId: Long, adId: Long) : Mono<Boolean>

    @Query("""
        select * 
        from ads
        where user_id = :userId
        ORDER BY created_at DESC
        limit :limit
        offset :offset
    """)
    fun paginationFindByUser (@Param("limit") limit: Int,
                              @Param("offset") offset: Int,
                              @Param("userId") userId: Long) : Flux<AdsTable>

    @Query("""
        select * 
        from ads
        where id IN (:ids)
    """)
    fun findByIds (@Param("ids") ids: List<Long>) : Flux<AdsTable>
}