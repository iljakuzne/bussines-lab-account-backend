package ru.era.lab_account.dao

import org.springframework.data.r2dbc.repository.Query
import org.springframework.data.r2dbc.repository.R2dbcRepository
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import ru.era.lab_account.domain.BussinesUser
import ru.era.lab_account.domain.view.KeyValueEntity

@Repository
interface BussinessUserRepository : R2dbcRepository<BussinesUser,Long> {

    @Query("""
        select * from
        bussiness_user
        where login = :login
    """)
    fun findByLogin (@Param("login") login: String): Mono<BussinesUser>

    @Query("""
        select exists (select id from bussiness_user where login = :login)
    """)
    fun existsUserByLogin (@Param("login") login: String) : Mono<Boolean>

    @Query("""
        select id as id,
        login as value
        from bussiness_user
        where id IN (:ids)
    """)
    fun findUserKeyValueByIds (@Param("ids") ids: List<Long>) : Flux<KeyValueEntity>
    

    @Query("""
        select *
        from bussiness_user
        where id IN (:ids)
    """)
    fun findUserByIds (@Param("ids") ids: List<Long>) : Flux<BussinesUser>
}