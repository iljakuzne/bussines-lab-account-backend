package ru.era.lab_account.dao

import org.springframework.data.r2dbc.repository.Query
import org.springframework.data.r2dbc.repository.R2dbcRepository
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import reactor.core.publisher.Flux
import ru.era.lab_account.domain.MarkCriteria
import ru.era.lab_account.models.project.ProjectStatus

@Repository
interface MarkCriteriaRepository : R2dbcRepository<MarkCriteria,Long> {

    @Query("""
        select * 
        from mark_criteria
        where criteria_status = :status
    """)
    fun getCriteriaMarkByStatus (@Param("status") status: ProjectStatus) : Flux<MarkCriteria>
}