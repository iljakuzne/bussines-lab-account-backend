package ru.era.lab_account.dao

import org.springframework.data.r2dbc.repository.Query
import org.springframework.data.r2dbc.repository.R2dbcRepository
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import ru.era.lab_account.domain.ProjectTemplate
import ru.era.lab_account.domain.view.ProjectCardRating
import ru.era.lab_account.domain.view.ProjectForMark
import ru.era.lab_account.models.project.ProjectStatus

@Repository
interface ProjectTemplateRepository : R2dbcRepository<ProjectTemplate,Long> {

    @Query(""" select * from 
        project
        ORDER BY create_date
        limit :limit
        offset :offset
    """)
    fun findAllProjectByPagination (@Param("limit") limit: Int,
                                    @Param("offset") offset: Int) : Flux<ProjectTemplate>


    @Query("""
        select exists (select id 
        from project
        where id = :projectId
        AND user_id = :userId)
    """)
    fun existsProjectByIdAndUserId (@Param("projectId") projectId: Long, @Param("userId") userId: Long) : Mono<Boolean>

    @Query("""
        select id as id,
        user_id as user_id,
        status as status
        from project
        where id = :projectId
    """)
    fun getProjectForMark (@Param("projectId") projectId: Long) : Mono<ProjectForMark>

    @Query("""
        update project
        set status = :status
        where id = :projectId
    """)
    fun updateStatus (@Param("status") status: ProjectStatus,
                      @Param("projectId") projectId: Long) : Mono<Void>

    @Query("""
        select * 
        from project
        where user_id = :userId
        ORDER BY create_date DESC
        limit :limit
        offset :offset
    """)
    fun getProjectsForUser (@Param("userId") userId: Long,
                            @Param("limit") limit: Int,
                            @Param("offset") offset: Int) : Flux<ProjectTemplate>

    @Query("""
        
        with count_criteria as (
            select criteria_status as criteria_status,
                    coalesce (count(id),0) as criteria_count
            from mark_criteria
            group by criteria_status
        )
        
        select p.* 
        from project p join count_criteria cc on p.status = cc.criteria_status
        where cc.criteria_count > (select count(m.id) from mark m where m.project_id = p.id and m.user_id = :userId)
        limit :limit
        offset :offset
    """)
    fun findProjectForStateholder (@Param("userId") userId: Long,
                                   @Param("limit") limit: Int,
                                   @Param("offset") offset: Int) : Flux<ProjectTemplate>


    @Query("""
        with projects as(
            select *
            from project
        ),
        
        sums as (
            select project_id, coalesce(SUM (mark),0) as summa
            from mark
            group by project_id
        )
        
        select p.id as id,
        p.user_id as user_id,
        bu.login as user_name,
        bu.email as user_email,
        bu.phone as user_phone,
        p.name as project_name,
        p.target as project_target,
        p.advantages as advantages,
        p.problem_solved as problem_solved,
        p.status as status,
        p.create_date as create_date,
        coalesce(sums.summa,0) as rating
        from projects p left join sums on p.id = sums.project_id
        left join bussiness_user bu on bu.id = p.user_id
        order by status,summa DESC

    """)
    fun getAllProjectWithRating () : Flux<ProjectCardRating>
}