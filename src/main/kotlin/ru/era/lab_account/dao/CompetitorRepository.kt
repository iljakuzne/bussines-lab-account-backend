package ru.era.lab_account.dao

import org.springframework.data.r2dbc.repository.Query
import org.springframework.data.r2dbc.repository.R2dbcRepository
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import reactor.core.publisher.Flux
import ru.era.lab_account.domain.Competitor

@Repository
interface CompetitorRepository : R2dbcRepository<Competitor,Long> {

    @Query("""
        select *
        from competitors
        where market_id = :marketId
    """)
    fun findByMarket (@Param("marketId") marketId: Long) : Flux<Competitor>
}