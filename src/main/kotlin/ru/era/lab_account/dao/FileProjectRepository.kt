package ru.era.lab_account.dao

import org.springframework.data.r2dbc.repository.Query
import org.springframework.data.r2dbc.repository.R2dbcRepository
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import reactor.core.publisher.Mono
import ru.era.lab_account.domain.FileProject

@Repository
interface FileProjectRepository : R2dbcRepository<FileProject,Long> {

    @Query("""
        UPDATE project_file
        SET hash = :hash,
        path = :path
        where id = :id
    """)
    fun updateFileProject (@Param("hash") hash: String,
                           @Param("path") path: String,
                           @Param("id") id: Long) : Mono<FileProject>


}