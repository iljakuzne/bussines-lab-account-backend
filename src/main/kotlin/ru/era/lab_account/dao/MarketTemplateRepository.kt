package ru.era.lab_account.dao

import org.springframework.data.r2dbc.repository.Query
import org.springframework.data.r2dbc.repository.R2dbcRepository
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import reactor.core.publisher.Mono
import ru.era.lab_account.domain.MarketTemplate


@Repository
interface MarketTemplateRepository : R2dbcRepository<MarketTemplate,Long> {

    @Query("""
        select * from
        market  
        where project_id = :projectId
    """)
    fun findByProjectId (@Param("projectId") projectId: Long) : Mono<MarketTemplate>
}