package ru.era.lab_account.dao

import org.springframework.data.r2dbc.repository.Query
import org.springframework.data.r2dbc.repository.R2dbcRepository
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import ru.era.lab_account.domain.ProductTemplate

@Repository
interface ProductTemplateRepository : R2dbcRepository<ProductTemplate, Long>{

    @Query("""
        select * 
        from product
        limit :limit,
        offset :offset
    """)
    fun findActualProduct (@Param("limit") limit: Int,
                           @Param("offset")offset: Int) : Flux<ProductTemplate>

    @Query("""
        select exists (select id from product where project_id = :projectId)
    """)
    fun existsByProjectId (@Param("projectId") projectId: Long) : Mono<Boolean>

    @Query("""
        select * from 
        product
        where project_id = :projectId
    """)
    fun findProductByProjectId (@Param("projectId") projectId: Long) : Mono<ProductTemplate>
}