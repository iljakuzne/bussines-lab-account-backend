package ru.era.lab_account.dao

import org.springframework.data.r2dbc.repository.Query
import org.springframework.data.r2dbc.repository.R2dbcRepository
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import reactor.core.publisher.Mono
import ru.era.lab_account.domain.Company

@Repository
interface CompanyRepository : R2dbcRepository<Company,Long> {
}