package ru.era.lab_account.dao

import org.springframework.data.r2dbc.repository.Query
import org.springframework.data.r2dbc.repository.R2dbcRepository
import org.springframework.data.repository.query.Param
import reactor.core.publisher.Flux
import ru.era.lab_account.domain.WaysAdvertising

interface WaysAdvertisingRepository : R2dbcRepository<WaysAdvertising, Long> {

    @Query("""
        select * 
        from ways_advertising
        where id IN (:ids)
    """)
    fun findWaysAdvertisingByIds (@Param("ids") ids: List<Long>) : Flux<WaysAdvertising>
}