package ru.era.lab_account.dao

import org.springframework.data.r2dbc.repository.Query
import org.springframework.data.r2dbc.repository.R2dbcRepository
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import reactor.core.publisher.Flux
import ru.era.lab_account.domain.UsingProduct

@Repository
interface UsingProductRepository : R2dbcRepository<UsingProduct, Long>{

    @Query("""
        select * 
        from using_product
        where id IN (:ids)
    """)
    fun findByIds (@Param("ids") ids: List<Long>) : Flux<UsingProduct>
}