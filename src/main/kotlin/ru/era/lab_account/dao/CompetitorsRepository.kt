package ru.era.lab_account.dao

import org.springframework.data.r2dbc.repository.R2dbcRepository
import org.springframework.stereotype.Repository
import ru.era.lab_account.domain.CompetitorsTemplate

@Repository
interface CompetitorsRepository :R2dbcRepository<CompetitorsTemplate,Long> {
}