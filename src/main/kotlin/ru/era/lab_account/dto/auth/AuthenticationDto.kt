package ru.era.lab_account.dto.auth

data class AuthenticationDto (val userLogin: String,
                              val userPassword: String)