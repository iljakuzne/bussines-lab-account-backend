package ru.era.lab_account.dto.auth

data class RegistrationDto(val login: String,
                           val password: String,
                           val phone: String,
                           val email: String,
                           val companyId: Long)