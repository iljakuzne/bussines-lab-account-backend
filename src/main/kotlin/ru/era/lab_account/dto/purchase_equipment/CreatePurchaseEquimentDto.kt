package ru.era.lab_account.dto.purchase_equipment


data class CreatePurchaseEquimentDto (val name: String,
                                      val cost: Double?)