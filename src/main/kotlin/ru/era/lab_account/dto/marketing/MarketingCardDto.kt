package ru.era.lab_account.dto.marketing


data class MarketingCardDto(val id: Long,
                            val productName: String,
                            val marketingProductCost: Double,
                            val volumeSales: Double,
                            val marketingCost: Double)