package ru.era.lab_account.dto.marketing

data class CreateMarketingDto (val projectId : Long,
                               val marketingProductCost : Double,
                               val volumeSales: Double,
                               val marketingCost: Double,
                               val typeSales: List<Long>,
                               val advertisingWays: List<Long>)