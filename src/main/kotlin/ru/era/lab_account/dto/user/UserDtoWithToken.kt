package ru.era.lab_account.dto.user

data class UserDtoWithToken (val userDto: UserDto,
                             val token: String)