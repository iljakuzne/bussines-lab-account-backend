package ru.era.lab_account.dto.user

import ru.era.lab_account.models.user.UserRole

data class UserDto (val id: Long,
                    val login: String,
                    val phone: String,
                    val email: String,
                    val role: UserRole)