package ru.era.lab_account.dto.market

import ru.era.lab_account.dto.competitor.CreateCompetitorDto

data class CreateMarketDto (val id: Long? = null,
                            val projectId: Long,
                            val barrierEntry: String,
                            val outputAnaliz: String,
                            val regionId: Long,
                            val competitors: List<CreateCompetitorDto>)