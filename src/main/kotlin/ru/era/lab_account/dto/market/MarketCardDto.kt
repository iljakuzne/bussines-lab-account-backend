package ru.era.lab_account.dto.market

data class MarketCardDto (val id: Long,
                          val competitorsName: String)