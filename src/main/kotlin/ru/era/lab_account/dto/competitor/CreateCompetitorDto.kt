package ru.era.lab_account.dto.competitor

data class CreateCompetitorDto (val id: Long? = null,
                                val competitorName: String,
                                val advantages: String,
                                val problemSolved: String)