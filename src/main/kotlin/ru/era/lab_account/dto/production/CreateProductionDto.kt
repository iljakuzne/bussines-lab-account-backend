package ru.era.lab_account.dto.production

import ru.era.lab_account.dto.purchase_equipment.CreatePurchaseEquimentDto
import ru.era.lab_account.dto.purchase_property.CreatePurchasePropertyDto
import ru.era.lab_account.dto.purchase_provider.CreatePurchaseProviderDto


data class CreateProductionDto (val projectId : Long,
                                val costPrise : Double,
                                val timeProduction : Double,
                                val createPurchaseEquimentDto: List<CreatePurchaseEquimentDto>,
                                val createPurchasePropertyDto: List<CreatePurchasePropertyDto>,
                                val creeatePurchaseProviderDto: List<CreatePurchaseProviderDto>)