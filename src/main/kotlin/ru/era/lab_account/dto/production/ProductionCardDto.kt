package ru.era.lab_account.dto.production

import ru.era.lab_account.domain.ProductionTemplate

data class ProductionCardDto (val id: Long,
                              val projectName : String,
                              val costPrise : Double,
                              val timeProduction : Double)