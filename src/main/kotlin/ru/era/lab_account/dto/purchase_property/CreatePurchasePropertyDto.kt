package ru.era.lab_account.dto.purchase_property


data class CreatePurchasePropertyDto (val name : String,
                                      val cost: Double?)