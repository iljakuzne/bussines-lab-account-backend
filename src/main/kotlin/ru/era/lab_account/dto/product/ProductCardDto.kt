package ru.era.lab_account.dto.product


data class ProductCardDto (val id: Long,
                           val productName: String,
                           val projectId: Long,
                           val productFunction: String,
                           val usingProductName: String)