package ru.era.lab_account.dto.product

data class CreateProductDto (val id: Long? = null,
                             val projectId: Long,
                             val productName: String,
                             val productFunctional: String,
                             val productCharacteristics: String?,
                             val usingProductId: Long,
                             val productAdvantages: String,
                             val additionalInformation: String?)
