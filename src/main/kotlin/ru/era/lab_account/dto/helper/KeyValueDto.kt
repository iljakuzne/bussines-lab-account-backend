package ru.era.lab_account.dto.helper

data class KeyValueDto (val id: Long,
                        val value: String)