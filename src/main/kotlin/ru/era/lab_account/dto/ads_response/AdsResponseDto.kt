package ru.era.lab_account.dto.ads_response


data class AdsResponseDto (val adsId: Long,
                           val adsName: String,
                           val userId: Long,
                           val userName: String,
                           val email: String,
                           val userPhone: String)