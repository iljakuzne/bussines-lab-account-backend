package ru.era.lab_account.dto.mark

data class MarkDto (val mark: Int,
                    val criteriaId: Long,
                    val criteriaName: String)