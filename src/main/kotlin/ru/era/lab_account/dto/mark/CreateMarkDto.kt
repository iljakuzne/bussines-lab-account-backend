package ru.era.lab_account.dto.mark

data class CreateMarkDto (val id: Long? = null,
                          val projectId: Long,
                          val mark: Int,
                          val criteriaId: Long)