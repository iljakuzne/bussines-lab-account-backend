package ru.era.lab_account.dto.product_with_project

import ru.era.lab_account.models.user.UserRole
import java.time.Instant

data class ProductWithProjectDto (val id: Long,
                                  val projectName: String,
                                  val projectTarget: String,
                                  val problemSolved: String,
                                  val advantages: String,
                                  val statusId: Int,
                                  val createDate: Instant,

                                  val productId: Long,
                                  val productName: String,
                                  val productFunctional : String,
                                  val productCharacteristic: String,
                                  val additionalInformation: String,
                                  val productAdvantages: String,


                                  val userId: Long,
                                  val username: String,
                                  val phone: String,
                                  val email: String,
                                  val role: UserRole)