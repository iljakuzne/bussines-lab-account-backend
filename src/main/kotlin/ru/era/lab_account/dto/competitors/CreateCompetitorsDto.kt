package ru.era.lab_account.dto.competitors

data class CreateCompetitorsDto (val competitorsName: String,
                                 val competitorsAdvantages: String?,
                                 val problemSolved: String?)