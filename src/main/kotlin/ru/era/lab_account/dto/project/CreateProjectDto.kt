package ru.era.lab_account.dto.project


data class CreateProjectDto (val id: Long? = null,
                             val projectName: String,
                             val projectTarget: String,
                             val solvedProblem: String?,
                             val advantages: String?,
                             val scopeId: Long,
                             val stageId: Long,
                             val readyTime: Int)