package ru.era.lab_account.dto.project


import java.time.Instant

data class ProjectRatingDto(val id: Long,
                            val userId: Long,
                            val userName: String,
                            val userEmail: String?,
                            val userPhone: String?,
                            val projectName: String,
                            val projectTarget: String,
                            val advantages: String?,
                            val problemSolved: String?,
                            val status: Int,
                            val createDate: Instant,
                            val rating: Int)