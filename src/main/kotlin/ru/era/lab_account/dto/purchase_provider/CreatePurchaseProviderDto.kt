package ru.era.lab_account.dto.purchase_provider

data class CreatePurchaseProviderDto (val name: String,
                                      val resources: Double?)