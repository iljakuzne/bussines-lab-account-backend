package ru.era.lab_account.dto.file

data class FileDto (val id: Long,
                    val name: String,
                    val hash: String)