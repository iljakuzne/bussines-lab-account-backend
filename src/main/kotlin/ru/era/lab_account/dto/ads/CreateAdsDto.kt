package ru.era.lab_account.dto.ads

data class CreateAdsDto (val name: String,
                         val content: String)