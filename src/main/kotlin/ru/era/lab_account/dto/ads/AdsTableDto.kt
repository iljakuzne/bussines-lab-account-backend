package ru.era.lab_account.dto.ads

import java.time.Instant

data class AdsTableDto (val id: Long,
                        val created: Instant,
                        val name: String,
                        val content: String,
                        val userId: Long,
                        val userName: String,
                        val userEmail: String,
                        val userPhone: String)