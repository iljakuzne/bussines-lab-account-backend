package ru.era.lab_account.dto.ads

import java.time.Instant


data class AdsTableDtoToUser (val id: Long,
                              val name: String,
                              val content: String,
                              val created: Instant)