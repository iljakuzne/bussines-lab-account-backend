CREATE TABLE bussiness_user(
    id serial NOT NULL PRIMARY KEY,
    login varchar(255) NOT NULL,
    first_name varchar(80),
    last_name varchar(80),
    second_name varchar(80),
    phone varchar(20),
    email varchar(40) NOT NULL,
    password varchar(255) NOT NULL,
    salt varchar(255) NOT NULL,
    role varchar(255) NOT NULL,
    UNIQUE(login),
    UNIQUE(email)
);