CREATE TABLE project_user_participant (
    id serial PRIMARY KEY NOT NULL,
    user_id int NOT NULL REFERENCES bussiness_user (id),
    role varchar(255) NOT NULL
);

ALTER TABLE market DROP COLUMN product_id;
DELETE FROM competitors;
DELETE FROM market;
ALTER TABLE market ADD COLUMN project_id int REFERENCES project (id) NOT NULL;