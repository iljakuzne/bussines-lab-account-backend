CREATE TABLE project_file (
    id serial NOT NULL PRIMARY KEY,
    file_name varchar(255) NOT NULL,
    hash varchar(256),
    content_type varchar(300) NOT NULL,
    path varchar(500) NOT NULL
);

