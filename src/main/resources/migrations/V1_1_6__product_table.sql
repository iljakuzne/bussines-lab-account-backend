CREATE TABLE using_product(
    id serial NOT NULL PRIMARY KEY,
    name varchar(255) NOT NULL
);

CREATE TABLE product(
    id serial NOT NULL PRIMARY KEY,
    project_id int REFERENCES project (id) NOT NULL,
    name varchar(255) NOT NULL,
    functional text NOT NULL,
    characteristics text,
    using_product_id int REFERENCES using_product (id) NOT NULL,
    advantages text NOT NULL,
    additional_information text
);

INSERT INTO using_product (name) VALUES ('Более 1 раза в неделю'),
                                        ('1 раз в неделю'),
                                        ('Более 1 раза в месяц'),
                                        ('1 раз в месяц'),
                                        ('Более 1 раза в год'),
                                        ('1 раз в год'),
                                        ('Менее 1 раза в год');