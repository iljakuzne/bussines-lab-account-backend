CREATE TABLE scope (
    id serial NOT NULL PRIMARY KEY,
    name varchar(255) NOT NULL
);

CREATE TABLE stage_realization(
    id serial NOT NULL PRIMARY KEY,
    stage_realization varchar(255) NOT NULL
);

CREATE TABLE project (
    id serial NOT NULL PRIMARY KEY,
    user_id int  REFERENCES bussiness_user (id) NOT NULL,
    name varchar(255) NOT NULL,
    target text NOT NULL,
    problem_solved text,
    advantages text,
    scope_id int REFERENCES scope(id) NOT NULL,
    stage_realization_id int REFERENCES stage_realization(id) NOT NULL,
    implement timestamp
);