ALTER TABLE type_sales_to_marketing ADD COLUMN id serial PRIMARY KEY;

CREATE TABLE ways_advertising(
    id serial NOT NULL PRIMARY KEY,
    name varchar(255)
);

CREATE TABLE ways_advertising_to_marketing(
    id serial PRIMARY KEY,
    marketing_id int REFERENCES marketing (id) ON DELETE CASCADE,
    ways_advertising_id int REFERENCES type_sales (id) ON DELETE CASCADE
);

CREATE INDEX ON ways_advertising_to_marketing (marketing_id, ways_advertising_id);

INSERT INTO ways_advertising (name) VALUES ('Видеореклама'), ('SEO'), ('Контекстная реклама'),
                                           ('Тизерная реклама'), ('SMM и реклама в соцсетях'),
                                           ('Статейное продвижение продуктов компаний'), ('Email-рассылка'),
                                           ('Ретаргетинг'), ('Вирусная реклама'),
                                           ('Реклама в мобильных приложениях'), ('Push-уведомления'),
                                           ('Реклама на радио'), ('Реклама в прессе'), ('Участие в конференциях'),
                                           ('Спонсорство');