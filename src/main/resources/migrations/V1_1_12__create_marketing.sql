CREATE TABLE type_sales(
    id serial NOT NULL PRIMARY KEY,
    name varchar(255)
);

INSERT INTO type_sales (name) VALUES ('Прямые продажи'), ('Телемаркетинг'), ('Корпоративный канал продаж'),
                                     ('Дилерский канал'), ('Партнерский канал'), ('Канал розничных продаж'),
                                     ('Сарафанное радио'), ('Реклама');

CREATE TABLE marketing(
    id serial NOT NULL PRIMARY KEY,
    product_id int REFERENCES product (id) NOT NULL,
    cost real NOT NULL,
    volume_sales real NOT NULL,
    market_cost real NOT NULL
);

CREATE TABLE type_sales_to_marketing(
    marketing_id int  REFERENCES marketing (id) ON DELETE CASCADE,
    type_sales_id int REFERENCES type_sales (id) ON DELETE CASCADE
);

CREATE TABLE production(
    id serial NOT NULL PRIMARY KEY,
    project_id int REFERENCES project (id) NOT NULL,
    cost_prise real NOT NULL,
    time_production real NOT NULL
);