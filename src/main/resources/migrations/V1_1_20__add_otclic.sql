CREATE TABLE response_ads (
    id serial PRIMARY KEY,
    user_id int  NOT NULL REFERENCES bussiness_user (id),
    ads_id int NOT NULL REFERENCES ads(id)
)