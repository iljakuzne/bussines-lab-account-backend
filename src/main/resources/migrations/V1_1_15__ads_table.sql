CREATE TABLE ads (
       id serial PRIMARY KEY,
       name varchar(255) not null,
       content varchar(2048) not null,
       created_at TIMESTAMP WITH TIME ZONE not null DEFAULT now()
)