CREATE TABLE equipment(
    id serial NOT NULL PRIMARY KEY,
    production_id int REFERENCES equipment (id) NOT NULL,
    name varchar(255) NOT NULL,
    cost real
);

CREATE TABLE property(
    id serial NOT NULL PRIMARY KEY,
    production_id int REFERENCES equipment (id) NOT NULL,
    name varchar(255) NOT NULL,
    cost real
);

CREATE TABLE provider(
    id serial NOT NULL PRIMARY KEY,
    production_id int REFERENCES provider (id) NOT NULL,
    name varchar(255) NOT NULL,
    resources real
);