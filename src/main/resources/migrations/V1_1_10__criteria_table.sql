CREATE TABLE mark_criteria (
    id serial NOT NULL PRIMARY KEY,
    criteria_name varchar(255) NOT NULL,
    criteria_status varchar(255) NOT NULL
);

CREATE TABLE mark (
    id serial NOT NULL PRIMARY KEY,
    user_id int NOT NULL REFERENCES bussiness_user (id),
    mark_criteria int NOT NULL REFERENCES mark_criteria (id),
    mark int NOT NULL DEFAULT 0
);

ALTER TABLE market ADD COLUMN region_id int NOT NULL DEFAULT 1;
ALTER TABLE market ADD CONSTRAINT  fk_region_market
FOREIGN KEY (region_id)
REFERENCES region(id);