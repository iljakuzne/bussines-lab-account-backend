CREATE TABLE company(
    id serial NOT NULL PRIMARY KEY,
    company_name varchar(255) NOT NULL
);

INSERT INTO company (company_name) VALUES ('Нет компании'), ('Рога и копыта'), ('Росатом'), ('Роснефть');

ALTER TABLE bussiness_user ADD COLUMN company_id int default 1;
ALTER TABLE bussiness_user ADD CONSTRAINT company_foreign_key FOREIGN KEY (company_id) REFERENCES company (id);

