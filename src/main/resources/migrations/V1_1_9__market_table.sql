CREATE TABLE market(
    id serial NOT NULL PRIMARY KEY,
    product_id int REFERENCES product (id) NOT NULL,
    barrier_entry text NOT NULL,
    output_analiz text NOT NULL
);

CREATE TABLE competitors(
    id serial NOT NULL PRIMARY KEY,
    market_id int NOT NULL REFERENCES market (id),
    name varchar(255) NOT NULL,
    advantages text,
    problem_solved text
);
